#include "iabGdprUserConsent.h"
#include "iabGdprUserConsentV1.h"
#include "base64_util.h"
#include "time_string.h"
#include <iostream>
#include <boost/thread/mutex.hpp>
#include <cmath>
#include <bitset>
#include "bidderConfig.h"
const unsigned int GDPR_VERSION_LENGTH = 6;
const unsigned int GDPR_CREATED_LENGTH = 36;
const unsigned int GDPR_UPDATED_LENGTH = 36;
const unsigned int GDPR_CMP_ID_LENGTH = 12;
const unsigned int GDPR_CMP_VERSION_LENGTH = 12;
const unsigned int GDPR_CONSENT_SCREEN_LENGTH = 6;
const unsigned int GDPR_CONSENT_LANGUAGE_LENGTH = 12;
const unsigned int GDPR_VENDOR_LIST_VERSION_LENGTH = 12;
const unsigned int GDPR_PURPOSES_ALLOWED_LENGTH = 24;
const unsigned int GDPR_MAX_VENDOR_ID_LENGTH = 16;
const unsigned int GDPR_ENCODING_TYPE_LENGTH = 1;
const uint32_t GDPR_BIT_ONE_PURPOSES = 1 << (GDPR_PURPOSES_ALLOWED_LENGTH - 1);

IabGdprUserConsentSingleton *IabGdprUserConsentSingleton::m_inst;

IabGdprUserConsentSingleton::IabGdprUserConsentSingleton() : m_bitFieldReverseOrder(false),
                                                             m_MMVendorID(0),
                                                             m_Primed(false),
                                                             m_mmRespectedPurposes(0),
                                                             m_userSyncRespectedPurposes(0)
{
    //inserts all GDPR objects
    insertAllGDPRConsents();
}

IabGdprUserConsentSingleton::~IabGdprUserConsentSingleton()
{
}

uint32_t IabGdprUserConsentV1::getPurposesIntFromIndex(const std::vector<uint16_t> &index)
{
    uint32_t purposes = 0;
    for (int purposeIndex : index)
    {
        if (purposeIndex == 0)
        {
            // it shouldn't start from 0 it should start from 1
            purposeIndex = 1;
        }
        uint32_t tmp = 1;
        // Reverse order
        tmp = tmp << (GDPR_PURPOSES_ALLOWED_LENGTH - purposeIndex);
        purposes |= tmp;
    }
    return purposes;
}

IabGdprUserConsentSingleton &IabGdprUserConsentSingleton::instance()
{
    if (m_inst == 0)
    {
        static boost::mutex s_lock;
        s_lock.lock();
        if (m_inst == 0)
        {
            m_inst = new IabGdprUserConsentSingleton();
        }
        s_lock.unlock();
    }
    return *m_inst;
}

void IabGdprUserConsentSingleton::insertGDPRDatum(GDPR::SlotName slotName, int length)
{
    //functions in this delimeter are called at bidder startup
    GDPR::Datum newDatum = GDPR::Datum(slotName, length);
    m_GDPRContainer.push_back(newDatum);
}

void IabGdprUserConsentV1::padBitSetWithZeros(boost::dynamic_bitset<>& bitset)
{
    int bitSetSize = bitset.size();
    int correctNumBytes = (bitSetSize + 7) / 8;
    int correctNumBits = correctNumBytes * 8;
    //resize does _not_ change the existing bits from 0...bitSetSize, so this is safe
    bitset.resize(correctNumBits);
}

void IabGdprUserConsentV1::convertBitSetToByteArray(const boost::dynamic_bitset<>& bitset, MM::Utils::MemBuffer &byteArray)
{
    int numBytes = bitset.size() / 8;
    for (int i = 0; i < numBytes; i++)
    {
        char currByte = 0;
        for (int j = i * 8; j < (i + 1) * 8; j++)
        {
            currByte <<= 1;
            currByte += (bitset.test(j) ? 1 : 0);
        }
        byteArray << currByte;
    }
}

std::string IabGdprUserConsentV1::generateGDPRString(const std::vector<int> &vendors, boost::dynamic_bitset<> &artificialDaisyBitSet)
{
    std::vector<GDPR::Datum> &GDPRKeys = IabGdprUserConsentSingleton::instance().m_GDPRContainer;
    int currIndexIntoBitVector = 0;
    for (GDPR::Datum slot : GDPRKeys)
    {
        GDPR::SlotName slotName = slot.slotName;
        //version:
        if (slotName == GDPR::SlotName::GDPR_VERSION)
        {
            uint8_t version = 1;
            setBitsFromLong(version, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //created
        else if (slotName == GDPR::SlotName::CREATED)
        {
            uint8_t created = 0;
            setBitsFromLong(created, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //updated
        else if (slotName == GDPR::SlotName::UPDATED)
        {
            uint8_t lastUpdated = 0;
            setBitsFromLong(lastUpdated, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //content manager id
        else if (slotName == GDPR::SlotName::CMP_ID)
        {
            uint8_t m_CMPID = 0;
            setBitsFromLong(m_CMPID, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //cmp  version
        else if (slotName == GDPR::SlotName::CMP_VERSION)
        {
            uint8_t m_CMPVersion = 0;
            setBitsFromLong(m_CMPVersion, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //consent screen
        else if (slotName == GDPR::SlotName::CONSENT_SCREEN)
        {
            uint8_t m_consentScreen = 0;
            setBitsFromLong(m_consentScreen, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //language
        else if (slotName == GDPR::SlotName::CONSENT_LANGUAGE)
        {
            setBitsFromLong('e', slot.length / 2, currIndexIntoBitVector, artificialDaisyBitSet);
            setBitsFromLong('n', slot.length / 2, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //version list version
        else if (slotName == GDPR::SlotName::VENDOR_LIST_VERSION)
        {
            uint8_t vendorListVersion = 0;
            setBitsFromLong(vendorListVersion, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //purposes allowed
        else if (slotName == GDPR::SlotName::PURPOSES_ALLOWED)
        {
            //when purposes is not give, and only
            //vendors are given, we need to assume full purposes
            long purposesAllowed = pow(2, 24) - 1;
            setBitsFromLong(purposesAllowed, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //max vendor id
        else if (slotName == GDPR::SlotName::MAX_VENDOR_ID)
        {
            long maxVendor = pow(2, 16) - 1;
            setBitsFromLong(maxVendor, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }

            //encoding type
        else if (slotName == GDPR::SlotName::ENCODING_TYPE)
        {
            uint8_t rangeFormat = 1;
            setBitsFromLong(rangeFormat, slot.length, currIndexIntoBitVector, artificialDaisyBitSet);
        }
    }
    //allocate the byte array
    MM::Utils::MemBuffer byteArray;
    appendVendorsRanges(vendors, currIndexIntoBitVector, artificialDaisyBitSet);
    //we need to round to multiple of 8 bits
    padBitSetWithZeros(artificialDaisyBitSet);
    convertBitSetToByteArray(artificialDaisyBitSet, byteArray);
//    return MM::Utils::base64Encode(byteArray.string(),
//                                   artificialDaisyBitSet.size() / 8, false);
    return ""; // we can ignore this for now
}

void IabGdprUserConsentSingleton::insertAllGDPRConsents()
{
    //don't insert into vector more than once
    if (m_Primed)
    {
        return;
    }
    insertGDPRDatum(GDPR::GDPR_VERSION, GDPR_VERSION_LENGTH);
    insertGDPRDatum(GDPR::CREATED, GDPR_CREATED_LENGTH);
    insertGDPRDatum(GDPR::UPDATED, GDPR_UPDATED_LENGTH);
    insertGDPRDatum(GDPR::CMP_ID, GDPR_CMP_ID_LENGTH);
    insertGDPRDatum(GDPR::CMP_VERSION, GDPR_CMP_VERSION_LENGTH);
    insertGDPRDatum(GDPR::CONSENT_SCREEN, GDPR_CONSENT_SCREEN_LENGTH);
    insertGDPRDatum(GDPR::CONSENT_LANGUAGE, GDPR_CONSENT_LANGUAGE_LENGTH);
    insertGDPRDatum(GDPR::VENDOR_LIST_VERSION, GDPR_VENDOR_LIST_VERSION_LENGTH);
    insertGDPRDatum(GDPR::PURPOSES_ALLOWED, GDPR_PURPOSES_ALLOWED_LENGTH);
    insertGDPRDatum(GDPR::MAX_VENDOR_ID, GDPR_MAX_VENDOR_ID_LENGTH);
    insertGDPRDatum(GDPR::ENCODING_TYPE, GDPR_ENCODING_TYPE_LENGTH);
    m_Primed = true;
}

bool IabGdprUserConsentSingleton::isVendorPurposesAllowed(uint32_t gvlId, uint32_t purposesToCheck) const
{
    //if we don't have entries for this id, return true.
    //We assume consent if vendor is absent for MVP
    auto purpIt = m_GDPRVendorsPurposes.find(gvlId);
    if (purpIt == m_GDPRVendorsPurposes.end())
    {
        return true;
    }
    return (purposesToCheck & purpIt->second) == purposesToCheck;
}

//Below functions are called for bid request traffic

void IabGdprUserConsentV1::setConsentStr(const char *consent, boost::dynamic_bitset<> &bitSet)
{
    //only increment the has daisy bit if it's not empty
    //because we don't respect daisy bits when they're empty
    m_consentStringBase64 = consent;
    const std::string decodedStr = expandBase64ToBinaryStr();
    joinGDPRMapAndTrafficString(decodedStr, bitSet);
}

IabGdprUserConsentV1::IabGdprUserConsentV1()
        : m_version(0),
          m_CMPVersion(0),
          m_CMPID(0),
          m_consentScreen(0),
          m_vendorListVersion(0),
          m_purposesAllowed(0),
          m_created(0),
          m_lastUpdated(0),
          m_maxVendorId(0),
          m_rangeFormat(false),
          m_rangeDefaultConsent(false),
          m_parsedDaisyBitSuccessfully(false)
{
}

long IabGdprUserConsentV1::getLongMoveIndex(boost::dynamic_bitset<> &bitSet, int offset, int &start) const
{
    long res = getLongAtPosition(bitSet, start, offset);
    //move pointer forward for the field, and then move pointer
    //for the 8 bit padding
    start += offset;
    return res;
}

void IabGdprUserConsentV1::setBitsFromLong(long long copyBitsFrom, int numLengthBits, int &startInd, boost::dynamic_bitset<> &bitSet)
{
    if (size_t(startInd + numLengthBits) > bitSet.size())
    {
        bitSet.resize(startInd + numLengthBits);
    }
    long toAndWith = pow(2, numLengthBits - 1);
    for (int i = 0; i < numLengthBits; i++)
    {
        if (copyBitsFrom & toAndWith)
        {
            //bits are set from high bits to low bits
            //so if we and with 1, a further number is higher
            bitSet.set(startInd);
        }
        toAndWith = toAndWith >> 1;
        startInd++;
    }
}

long IabGdprUserConsentV1::getLongAtPosition(boost::dynamic_bitset<> &bitSet, int start, int offset) const
{
    if ((start + offset) >= (int)bitSet.size())
    {
        return -1;
    }
    long longRes = 0;
    //capture set bits from start to offset
    //going backwards, because we read binary right to left
    for (int i = start; i < start + offset; i++)
    {
        longRes <<= 1;
        if (bitSet.test(i))
        {
            longRes |= 1;
        }
    }
    return longRes;
}

bool IabGdprUserConsentV1::joinGDPRMapAndTrafficString(const std::string& consentStr, boost::dynamic_bitset<> &optionalBitsetFromNonIABTraffic)
{
    //minimum length of daisy bit
    if ((consentStr.size() * 8) < 173)
    {
        m_parsedDaisyBitSuccessfully = false;
        return m_parsedDaisyBitSuccessfully;
    }
    std::vector<GDPR::Datum> &GDPRKeys = IabGdprUserConsentSingleton::instance().m_GDPRContainer;
    uint32_t numConsentBits = consentStr.size() * 8;
    //if we aren't given a daisy bit in the input,
    //this a normal IAB OpenRTB request, and we need to allocate
    //space. Otherwise we can just point the shared ptr to the incoming one
    boost::dynamic_bitset<> toFillIfEmpty;
    boost::dynamic_bitset<> &daisyBitSet = (optionalBitsetFromNonIABTraffic.size() ? optionalBitsetFromNonIABTraffic : toFillIfEmpty);
    if (!optionalBitsetFromNonIABTraffic.size())
        toFillIfEmpty.resize(numConsentBits);
    IabGdprUserConsent::convertBytesToBitset(consentStr, daisyBitSet);
    int currIndexIntoBitVector = 0;
    for (GDPR::Datum slot : GDPRKeys)
    {
        int length = slot.length;
        long gdprValue = getLongAtPosition(daisyBitSet, currIndexIntoBitVector, length);
        GDPR::SlotName slotName = slot.slotName;
        //version:
        if (slotName == GDPR::SlotName::GDPR_VERSION)
        {
            m_version = (uint8_t)gdprValue;
        }

            //created
        else if (slotName == GDPR::SlotName::CREATED)
        {
            m_created = gdprValue;
        }

            //updated
        else if (slotName == GDPR::SlotName::UPDATED)
        {
            m_lastUpdated = gdprValue;
        }

            //content manager id
        else if (slotName == GDPR::SlotName::CMP_ID)
        {
            m_CMPID = (uint16_t)gdprValue;
        }

            //cmp  version
        else if (slotName == GDPR::SlotName::CMP_VERSION)
        {
            m_CMPVersion = (uint8_t)gdprValue;
        }

            //consent screen
        else if (slotName == GDPR::SlotName::CONSENT_SCREEN)
        {
            m_consentScreen = (uint8_t)gdprValue;
        }

            //language
        else if (slotName == GDPR::SlotName::CONSENT_LANGUAGE)
        {
            //first character is 6 bits
            const char firstLetter = (char)getLongAtPosition(daisyBitSet, currIndexIntoBitVector, 6) + 'a';
            const char secondLetter = (char)getLongAtPosition(daisyBitSet, currIndexIntoBitVector + 6, 6) + 'a';
            m_consentLanguage += firstLetter;
            m_consentLanguage += secondLetter;
        }

            //version list version
        else if (slotName == GDPR::SlotName::VENDOR_LIST_VERSION)
        {
            m_vendorListVersion = (uint16_t)gdprValue;
        }

            //purposes allowed
        else if (slotName == GDPR::SlotName::PURPOSES_ALLOWED)
        {
            m_purposesAllowed = (uint32_t)gdprValue;
        }

            //max vendor id
        else if (slotName == GDPR::SlotName::MAX_VENDOR_ID)
        {
            m_maxVendorId = (uint32_t)gdprValue;
        }

            //encoding type
        else if (slotName == GDPR::SlotName::ENCODING_TYPE)
        {
            m_rangeFormat = (gdprValue == 1);
        }
        //move forward curr index ptr
        currIndexIntoBitVector += length;
    }
    //get a char ptr that we can move around easily with ++ operations as we look at it
    m_parsedDaisyBitSuccessfully = populateGDPRVendors(daisyBitSet, currIndexIntoBitVector, m_maxVendorId);
    return m_parsedDaisyBitSuccessfully;
}

const std::string &IabGdprUserConsentV1::getOriginalGDPRStr() const { return m_consentStringBase64; }

const std::string IabGdprUserConsentV1::expandBase64ToBinaryStr()
{
    if (m_consentStringBase64.empty())
    {
        return "";
    }
    return MM::Utils::base64Decode(m_consentStringBase64, true);
}

#define VENDOR_ID_LEN 16
#define SINGLE_OR_RANGE_LEN 1
#define DEFAULT_CONSENT_LEN 1
#define NUM_ENTRIES_LENGTH 12
//always append range
void IabGdprUserConsentV1::appendVendorsRanges(const std::vector<int> &consentingVendors, int currIndexIntoBitset, boost::dynamic_bitset<>& bitset)
{
    int numVendors = consentingVendors.size();
    //default consent. 0 because the vector of vendors is the consenting vendors. Anyone outside the vector is _not_ consenting.
    setBitsFromLong(0, DEFAULT_CONSENT_LEN, currIndexIntoBitset, bitset);

    //number of entries
    setBitsFromLong(numVendors, NUM_ENTRIES_LENGTH, currIndexIntoBitset, bitset);
    //don't append a proper range if there isn't one.
    if (!numVendors)
        return;

    //multi range (1 for yes, we are including all the vendors in this range)
    setBitsFromLong(1, SINGLE_OR_RANGE_LEN, currIndexIntoBitset, bitset);

    //start vendor id
    setBitsFromLong(consentingVendors[0], VENDOR_ID_LEN, currIndexIntoBitset, bitset);

    //end vendor id
    setBitsFromLong(consentingVendors[numVendors - 1], VENDOR_ID_LEN, currIndexIntoBitset, bitset);

    for (int i = 0; i < numVendors; i++)
    {
        setBitsFromLong(consentingVendors[i], VENDOR_ID_LEN, currIndexIntoBitset, bitset);
    }
}

bool IabGdprUserConsentV1::parseGDPRRangeEntry(boost::dynamic_bitset<> &rangeEntry, int maxVendor, int &index)
{
    //multiple items in the range
    long multiRange = getLongMoveIndex(rangeEntry, SINGLE_OR_RANGE_LEN, index);
    //the actual vendor information is after the flag, so move the pointer, but check before we move
    if (multiRange == -1)
        return false;
    if (!multiRange)
    {
        long vendorIDNum = getLongMoveIndex(rangeEntry, VENDOR_ID_LEN, index);
        //we need to be able to read first vendor
        if (vendorIDNum == -1)
            return false;

        //check for max vendor
        if (vendorIDNum > maxVendor)
            return false;

        //add vendor to map
        m_GDPRVendorsConsent[vendorIDNum] = !m_rangeDefaultConsent;
    }
    else
    {
        long firstVendor = getLongMoveIndex(rangeEntry, VENDOR_ID_LEN, index);
        //need to be able to read end vendor instruction
        if (firstVendor == -1)
        {
            return false;
        }
        long lastVendor = getLongMoveIndex(rangeEntry, VENDOR_ID_LEN, index);
        //need to be able to start reading actual vendor list
        if (lastVendor == -1)
        {
            return false;
        }
        long currVendor = firstVendor;
        if (currVendor > maxVendor)
            return false;

        for (int i = firstVendor; i <= lastVendor; i++) {
            m_GDPRVendorsConsent[i] = !m_rangeDefaultConsent;
        }
    }
    return true;
}

//macros to work with range extension
bool IabGdprUserConsentV1::populateGDPRVendorsRange(boost::dynamic_bitset<> &vendors, int index, int maxVendor)
{
    //we are reading num vendors for 13 bits, so need to have that much left
    long defaultConsent = getLongMoveIndex(vendors, DEFAULT_CONSENT_LEN, index);
    if (defaultConsent == -1)
    {
        return false;
    }
    m_rangeDefaultConsent = (defaultConsent == 1);
    //+1 because string constructor is exclusive, and move one more for
    int numRanges = getLongMoveIndex(vendors, NUM_ENTRIES_LENGTH, index);
    if (numRanges == -1)
    {
        return false;
    }
    //move past num vendors
    for (int i = 0; i < numRanges; i++)
    {
        bool suc = parseGDPRRangeEntry(vendors, maxVendor, index);
        if (!suc)
            return false;
    }
    return true;
}

bool IabGdprUserConsentV1::populateGDPRVendors(boost::dynamic_bitset<> &vendorsBeg, int index, uint32_t maxVendorID)
{
    if (m_rangeFormat)
    {
        if (!populateGDPRVendorsRange(vendorsBeg, index, maxVendorID))
        {
            return false;
        }
    }
    else
    {
        if (!populateGDPRVendorsBit(vendorsBeg, index, maxVendorID))
        {
            return false;
        }
    }
    return true;
}

bool IabGdprUserConsentV1::populateGDPRVendorsBit(boost::dynamic_bitset<> &vendors, int index, int maxVendors)
{
    int endOfBitSet = vendors.size();
    if (index + maxVendors > endOfBitSet)
    {
        return false;
    }
    bool isBitFieldReverseOrder = IabGdprUserConsentSingleton::instance().m_bitFieldReverseOrder;
    for (int i = 0; i < maxVendors; i++)
    {
        //binary 1 is consenting in bitfield format
        if (vendors[index + i] == 1)
        {
            //this vendor is consenting
            //string is read right to left, so we need the offset below
            int curVendorId = i + 1;
            if (isBitFieldReverseOrder)
            {
                curVendorId = maxVendors - i;
            }
            //only insert consenting vendors to save space
            m_GDPRVendorsConsent[curVendorId] = true;
        }
    }
    return true;
}

bool IabGdprUserConsentV1::isCMPIDAllowed() const
{
    const BidderConfig::GDPR &gdpr = BidderConfig::instance().getGDPR();
    // finding from vector because expected number of ids are 1 or 2
    // So hash_set or set are not better solution
    auto it = std::find(gdpr.bannedCMPIDs.begin(), gdpr.bannedCMPIDs.end(), m_CMPID);
    if (it != gdpr.bannedCMPIDs.end())
    {
        return false;
    }
    return true;
}

bool IabGdprUserConsentV1::hasVendorConsent(uint32_t gvlId) const
{
    auto consentIt = m_GDPRVendorsConsent.find(gvlId);
    //if consent is absent, it's not always
    //a reason to return false
    if (consentIt == m_GDPRVendorsConsent.end())
    {
        if (m_rangeFormat)
        {
            if (!m_rangeDefaultConsent)
            {
                return false;
            }
            //for bitfield, if vendor is absent, there's no consent
        }
        else
        {
            return false;
        }
    }
    else
    {
        //we can dereference iterator safely because we check count above
        bool consent = consentIt->second;
        //if consent is there, respect it
        if (!consent)
        {
            return false;
        }
    }
    return true;
}

bool IabGdprUserConsentV1::isVendorAllowedToBid(const uint32_t gvlId) const
{
    const auto &gdprConfig = IabGdprUserConsentSingleton::instance();
    uint32_t purposesToCheck = gdprConfig.m_mmRespectedPurposes;
    return isPurposesAllowed(purposesToCheck) && isCMPIDAllowed() &&
           gdprConfig.isVendorPurposesAllowed(gvlId, purposesToCheck) &&
           hasVendorConsent(gvlId);
}

bool IabGdprUserConsentV1::isVendorUserSyncAllowed(const uint32_t gvlId) const
{
    const auto &gdprConfig = IabGdprUserConsentSingleton::instance();
    uint32_t purposesToCheck = gdprConfig.m_userSyncRespectedPurposes;
    return isPurposesAllowed(purposesToCheck) && isCMPIDAllowed() &&
           ((gvlId == 0 && BidderConfig::instance().getGDPR().allowUserSyncIfNoGvlId) ||
            (gdprConfig.isVendorPurposesAllowed(gvlId, purposesToCheck) && hasVendorConsent(gvlId)));
}

void IabGdprUserConsentV1::report(MM::Utils::MemBuffer& output) const
{
    std::bitset<GDPR_PURPOSES_ALLOWED_LENGTH> purposesAllowedBits(m_purposesAllowed);

    output << "Version: <code>" << (unsigned int)m_version << "</code><br>\n"
           << "CMPVersion: <code>" << (unsigned int)m_CMPVersion << "</code><br>\n"
           << "CMPID: <code>" << (unsigned int)m_CMPID << "</code><br>\n"
           << "ConsentScreen: <code>" << (unsigned int)m_consentScreen << "</code><br>\n"
           << "VendorListVersion: <code>" << (unsigned int)m_vendorListVersion << "</code><br>\n"
           << "MaxVendorId: <code>" << (unsigned int)m_maxVendorId << "</code><br>\n"
           << "PurposesAllowed: <code>" << m_purposesAllowed << "("
           << purposesAllowedBits.to_string() << ")</code><br>\n"
           << "Created: <code>" << m_created << "("
           << MM::Utils::TimeString().gmTimeForHTML((size_t)(m_created / 10.0)) << ")</code><br>\n"
           << "LastUpdated: <code>" << m_lastUpdated << "("
           << MM::Utils::TimeString().gmTimeForHTML((size_t)(m_lastUpdated / 10.0)) << ")</code><br>\n"
           << "ConsentLanguage: <code>" << m_consentLanguage << "</code><br>\n"
           << "RangeFormat: <code>" << (m_rangeFormat ? "True" : "False") << "</code><br>\n"
           << "RangeDefaultConsent: <code>" << (m_rangeDefaultConsent ? "True" : "False") << "</code><br>\n"
           << "ParsedDaisyBitSuccessfully: <code>"
           << (m_parsedDaisyBitSuccessfully ? "True" : "False") << "</code><br>\n"
           << "GDPRVendorsConsent: ";
    if (!m_rangeFormat)
    {
        output << "<code>";
        for (uint16_t itr = m_maxVendorId; itr > 0; --itr)
        {
            auto itrConsentItem = m_GDPRVendorsConsent.find(itr);
            if (itrConsentItem != m_GDPRVendorsConsent.end())
            {
                output << (itrConsentItem->second ? '1' : '0');
            }
            else
            {
                output << '0';
            }
        }
        output << "</code>";
    }

    output << "<br>\n";
    uint16_t itr = 0;
    for (const auto &consentItem : m_GDPRVendorsConsent)
    {
        output << itr << " - vendor id: <code>" << consentItem.first << " = "
               << (consentItem.second ? "True" : "False") << "</code><br>\n";
        ++itr;
    }
}