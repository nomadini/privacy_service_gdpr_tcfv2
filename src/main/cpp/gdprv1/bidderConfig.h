//
// Created by Mahmoud Taabodi on 4/2/20.
//

#ifndef PRIVACY_SERVICE_GDPR_TCFV2_BIDDERCONFIG_H
#define PRIVACY_SERVICE_GDPR_TCFV2_BIDDERCONFIG_H


class BidderConfig {


public:


    struct GDPR {
        bool enableBiddingBlockOnMMVendorID;
        bool enableV2ConsentStringProcessing;
        bool decodeOptionalParts;
        bool enableBiddingBlockOnAllVendors;
        bool useTransitionLogicV2;
        bool pickUpGDPR;
        bool logGDPRDaisyBit;
        bool logGDPRSignal;
        bool bitFieldReverseOrder;
        bool bidOnBadDaisybit;
        bool enableUserSyncCheckBitOne;
        bool allowUserSyncIfNoGvlId;
        bool allowUserSyncBadDaisyBit;
        uint32_t MMVendorIDIAB;
        uint32_t MMVendorIDIAB_V2;
        uint32_t MMVendorIDAdx;
        std::string macroExtension;
        std::vector<uint16_t> mmRespectedPurposes;
        std::vector<uint16_t> userSyncRespectedPurposes;
        std::vector<uint16_t> bannedCMPIDs;
        GDPR() :
                enableBiddingBlockOnMMVendorID(false),
                enableV2ConsentStringProcessing(false),
                decodeOptionalParts(false),
                enableBiddingBlockOnAllVendors(false),
                useTransitionLogicV2(false),
                pickUpGDPR(false),
                logGDPRDaisyBit(false),
                logGDPRSignal(false),
                bitFieldReverseOrder(false),
                bidOnBadDaisybit(true),
                enableUserSyncCheckBitOne(false),
                allowUserSyncIfNoGvlId(false),
                allowUserSyncBadDaisyBit(true),
                MMVendorIDIAB(0),
                MMVendorIDIAB_V2(0),
                MMVendorIDAdx(0),
                macroExtension("&gdpr=[BID_ATTR.gdpr_flag]&gdpr_consent=[BID_ATTR.gdpr_str]")
        {
            userSyncRespectedPurposes.push_back(1);
        }
    };


    GDPR                m_GDPR;
    const GDPR& getGDPR() const {return m_GDPR;}

    static BidderConfig &instance() {
        auto m_inst = new BidderConfig();
        return *m_inst;
    }
};

#endif //PRIVACY_SERVICE_GDPR_TCFV2_BIDDERCONFIG_H
