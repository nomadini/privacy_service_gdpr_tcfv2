/*
 * Class responsible for providing utility functions for business logic
 * for GDPR. For now, we are using this class as an attachment on a bid opportunity
 *
 * Design in general is to parse string contains 0's and 1's. We could have used a BigInt class
 * combined with some binary arithmetic instead, but BigInts are expensive and complicated, and parsing a 1000 character string is not
 * complicated or expensive
 */

#ifndef IAB_GDPR_V1_USER_CONSENT
#define IAB_GDPR_V1_USER_CONSENT

#include <boost/dynamic_bitset.hpp>
#include <gtest/gtest_prod.h>
#include <map>
#include "privacy_service.h"

//common GDPR namespace declarations
//for use by GDPR classes
namespace GDPR
{
    typedef enum
    {
        INVALID,
        GDPR_VERSION,
        CREATED,
        UPDATED,
        CMP_ID,
        CMP_VERSION,
        CONSENT_SCREEN,
        CONSENT_LANGUAGE,
        VENDOR_LIST_VERSION,
        PURPOSES_ALLOWED,
        MAX_VENDOR_ID,
        ENCODING_TYPE,
        MAX_DIM
    } SlotName;

    struct Datum
    {
        int length;
        SlotName slotName;
        Datum() : length(0),
                  slotName(SlotName::INVALID)
        {
        }

        Datum(SlotName name, int length) : length(length),
                                           slotName(name)
        {
        }
    };

    struct Vendor
    {
        uint32_t vendorID;
        uint32_t vendorPurposes;
        bool doesConsent;
        Vendor() : vendorID(0),
                   vendorPurposes(0),
                   doesConsent(false)
        {
        }
    };
}; // namespace GDPR

//GDPR class that is an attachment to the bid request. The state and global static functions and members
//are in the class below
class IabGdprUserConsentV1
{

    friend class IabGdprUserConsentSingleton;
    FRIEND_TEST(GDPR_Base_Too_Short, 1);
    FRIEND_TEST(GDPR_BitField_Too_Short, 1);
    FRIEND_TEST(GDPR_BitField_Too_Long, 1);
    FRIEND_TEST(GDPR_Range_Too_Short, 1);
    FRIEND_TEST(GDPR_Range_Too_Long, 1);
    FRIEND_TEST(GDPR_Good_BitField, 1);
    FRIEND_TEST(GDPR_Good_Range_Single, 1);
    FRIEND_TEST(GDPR_MultiRange_With_one_entry, 1);
    FRIEND_TEST(GDPR_Long_At_Pos, 1);
    FRIEND_TEST(GDPR_Long_Move_Ind, 1);
    FRIEND_TEST(GDPR_Long_Move_Multiple, 1);
    FRIEND_TEST(GDPR_Bytes_To_Bitset, 1);
    FRIEND_TEST(GDPR_Long_To_Bitset, 1);
    FRIEND_TEST(GDPR_PadBitSet, 1);
    FRIEND_TEST(MAKE_GDPR_STRING_AND_DESERIALIZE_CONSENT, 1);
    FRIEND_TEST(MAKE_GDPR_STRING_AND_DESERIALIZE_NO_CONSENT, 1);
    FRIEND_TEST(GDPR_VENDORS_APPEND, 1);
    FRIEND_TEST(GDPR_range_parsing, 1);
    //constraint to parse the consent string against
public:
    IabGdprUserConsentV1();

    //request needs to respect gdpr
    void setGDPR(bool val);

    void setConsentStr(const char *inp, boost::dynamic_bitset<> &optionalBitsetFromNonIABTraffic);
    uint8_t getVersion() const { return m_version; }
    time_t getCreated() const { return m_created; }
    time_t getLastUpdated() const { return m_lastUpdated; }
    uint16_t getCMPID() const { return m_CMPID; }
    uint8_t getConsentScreen() const { return m_consentScreen; }
    const std::string &getConsentLanguage() const { return m_consentLanguage; }
    uint16_t getVendorListVersion() const { return m_vendorListVersion; }
    bool getGDPRSuccessful() const { return m_parsedDaisyBitSuccessfully; }
    bool isVendorAllowedToBid(const uint32_t gvlId) const;
    bool isVendorUserSyncAllowed(const uint32_t gvlId) const;

    static std::string generateGDPRString(const std::vector<int> &vendors, boost::dynamic_bitset<> &artificialDaisyBitSet);

    void report(MM::Utils::MemBuffer& output) const;
    const std::string &getOriginalGDPRStr() const;

    static uint32_t getPurposesIntFromIndex(const std::vector<uint16_t> &index);

    bool isPurposesAllowed(uint32_t purposesToCheck) const { return (purposesToCheck & m_purposesAllowed) == purposesToCheck; }
    bool hasVendorConsent(uint32_t gvlId) const;
    bool isCMPIDAllowed() const;

    //get the long from the bitset in the given range [start, offset)
    //then move the start pointer by offset
    long getLongMoveIndex(boost::dynamic_bitset<> &bitSet, int offset, int &start) const;

    //get the long from the bitset in the given range [start, offset)
    long getLongAtPosition(boost::dynamic_bitset<> &bitSet, int start, int end) const;

    //used to pad a bitset with an 8-bit multiple of bits, so that when we call
    //convertBytes.... we can correctly get a byte-aligned bitset
    static void padBitSetWithZeros(boost::dynamic_bitset<>& bitset);

    //converts the bitset to the byteArray by iterating over the bits in groups of 8 bits.
    static void convertBitSetToByteArray(const boost::dynamic_bitset<>& bitset, MM::Utils::MemBuffer &byteArray);

    //the opposite of getLongMoveIndex, this function sets the corresponding bits in bitSet
    //from "from", and mvoes "start" by "offset"
    static void setBitsFromLong(long long copyBitsFrom, int numLengthBits, int &startInd, boost::dynamic_bitset<> &bitSet);

    //populates GDPR object with privacy metadata
    //Returns:
    //False if input is not big enough or too small
    //This isn't a fixed size, but variable depending on input
    bool joinGDPRMapAndTrafficString(const std::string& consentStr, boost::dynamic_bitset<> &optionalBitsetFromNonIABTraffic);

    const std::string expandBase64ToBinaryStr();

    //*Parent function to parse vendors*
    //iteratively moves forward bitset ptr (index) and reads the metadata.
    //Makes sure that if a vendor picked up is bigger than maxVendor
    //there is an inconsistency, and returns false
    bool populateGDPRVendors(boost::dynamic_bitset<> &vendorsBeg, int index, uint32_t maxVendor);

    //returns false if the number of elements is inconsisent with the length of the string
    //OR if the same can be said of a range
    //This function parses _ALL_ ranges in the bitset
    //index refers to where we are in the bitset.
    //max vendor is the maximum vendor as read from the daisy bit earlier
    bool populateGDPRVendorsRange(boost::dynamic_bitset<> &vendors, int index, int maxVendor);

    static void appendVendorsRanges(const std::vector<int> &consentingVendors, int currIndexIntoBitset, boost::dynamic_bitset<> &toAppend);

    //returns true if there were no inconsistencies in this range entry, and it was parsable
    //Max vendor is maximum vendor id for this daisy bit, and
    bool parseGDPRRangeEntry(boost::dynamic_bitset<> &rangeEntry, int maxVendor, int &index);

    //returns false if the number of elements is inconsisent with the length of the bitset
    //OR if the same can be said of a range
    //This function iterates from 0 to maxVendors and checks consent for each
    //Index is the position in the bitset at the time of calling this function.
    bool populateGDPRVendorsBit(boost::dynamic_bitset<> &vendors, int index, int maxVendor);

    //base64 encoded vendor consent cookie
    //this class decodes it, and splits it up into its components
    std::string m_consentStringBase64;

    /* GDPR Fields */
    uint8_t m_version;
    uint8_t m_CMPVersion;
    uint16_t m_CMPID;
    uint8_t m_consentScreen;
    uint16_t m_vendorListVersion;
    uint32_t m_purposesAllowed;
    time_t m_created;
    time_t m_lastUpdated;
    uint16_t m_maxVendorId;
    std::string m_consentLanguage;
    bool m_rangeFormat;
    bool m_rangeDefaultConsent;
    bool m_parsedDaisyBitSuccessfully;

    std::map<uint32_t, bool> m_GDPRVendorsConsent;
};

//class to represent all the global GDPR state and singleton methods of GDPR
class IabGdprUserConsentSingleton
{
    friend class IabGdprUserConsent;
    friend class IabGdprUserConsentV1;
    friend class MM::Privacy::PrivacyService;
    friend class MM::Privacy::TCFV2Decoder;
    FRIEND_TEST(TEST_JSON_SERIALIZATION_V2, TEST_OUTPUT);
    FRIEND_TEST(TEST_CREATING_ADMIN_PAGE, TEST_OUTPUT);
    FRIEND_TEST(VERIFY_PARSING_MULTI_RANGE_CASE_IN_VENDOR_CONSENTS, PARSE_STRING);
    FRIEND_TEST(VERIFY_PARSING_STRING_WITH_PUB_RESTRICTIONS, PARSE_STRING);
    FRIEND_TEST(VERIFY_PARSING_MOST_FIELDS, PARSE_CORE_STRING);
    FRIEND_TEST(PARSE_VENDOR_ALLOWED, PARSE_VENDORS_STRING);

public:
    IabGdprUserConsentSingleton();
    ~IabGdprUserConsentSingleton();
    static IabGdprUserConsentSingleton &instance();
    const uint32_t getMMVendor() { return m_MMVendorID; }
    //insert the objects that specify where each GDPR datum is (e.g., what bits "Version" occupies
    void insertAllGDPRConsents();

public:
    bool isVendorPurposesAllowed(uint32_t gvlId, uint32_t purposesToCheck) const;

    std::vector<GDPR::Datum> m_GDPRContainer;

    bool m_bitFieldReverseOrder;
    static IabGdprUserConsentSingleton *m_inst;

    bool enableV2ConsentStringProcessing = false;
    bool decodeOptionalParts = false;
    bool useTransitionLogicV2 = false;

    uint32_t m_MMVendorID;
    uint32_t m_MMVendorID_V2 = 0;

    bool m_Primed;
    // stores vendors purpose
    // temporarily empty
    // todo: load from gvl file
    std::unordered_map<uint32_t, uint32_t> m_GDPRVendorsPurposes;

    uint32_t m_mmRespectedPurposes;
    uint32_t m_userSyncRespectedPurposes;

    //inserts gdpr datum into our map.
    void insertGDPRDatum(GDPR::SlotName slotName, int length);

    uint32_t MMPurposes;
};

#endif
