////
//// Created by Mahmoud Taabodi on 3/4/20.
////
//
#include "gtest/gtest.h"
#include "iabGdprUserConsentV1.h"

TEST(GPDR_FAILING, 1)
{
std::string test = "BOwxCfYOwxCfYCPABBENBP-AAA___A";
IabGdprUserConsentV1 consent;
IabGdprUserConsentSingleton::instance().insertAllGDPRConsents();
boost::dynamic_bitset<> dummy;
consent.setConsentStr(test.c_str(), dummy);
}

TEST(GPDR_FAILING_1, 1)
{
std::string test = "BOxFygCOxFygC____AENCv-AAAAsiEmAACAAIAAYABAADAAIAASAAoABYADAAGgAOAAeABAACQAFQALAAXAAwABkADYAHAAPAAfABAACIAEgAJQATgApABUACwAFoALwAYAAxABkADMAGgAOQAdgA8AB6AD4AH4AQQAhgBEACKAEcAJAASQAmABNACcAE8AKAAUQApABTACoAFUAKwAVwAsABaAC2AFwALoAXgAvgBhADEAGUANAAaQA2gBuADgAHEAOQAcwA7gB8AD6AH4AP4AgABBACEAEMAIkARYAjABHACPAEmAJQASoAlgBLgCYAE2AJwAToAnwBQgCiAFGAKQAUoAqABVACtAFcALcAXAAuQBeAC_AGAAMEAYQAwwBiADIAGSAM0AZwA0ABogDUAGqANYAbIA2gBwADhAHEAOMAcoA5wB0ADqAHWAO0AdwA8QB5ADzAHqAPYAfQA_QCAAECAIKAQYAhABCQCFAEMAIgAREAiYBFgCMgEcAI6ASIAkwBJwCUgEqAJWAS0AlwBMACaAE1AJuATgAnQBOwCeAE9AJ-AUUApABTQCoAFSAKmAVQAqoBWQCugFlALMAWcAtoBcQC6gF6AL6AX4AwABgQDCAGGAMUAY0AyYBlQDLgGZAM0AZ0Az4BoADTgGoANYAa0A1wBrwDZAG7AN-AcIA4gBxwDlAHSAOmAdQA6oB2gDtgHfAPIAeYA84B6gD3AHwAPmAfQA-wB-gD-AH_AQAAgQBAwCCgEHgIQAQuAhgBDoCHwEQAIpARYAi8BHgCQAEhgJIASWAlUBL4CZAEygJrATYAnABOICdgE7gKRAUmApsBUACpQFWAKwAVkArMBXICvgFhALI";
IabGdprUserConsentV1 consent;
IabGdprUserConsentSingleton::instance().insertAllGDPRConsents();
boost::dynamic_bitset<> dummy;
consent.setConsentStr(test.c_str(), dummy);
}

TEST(RANGE_PARSING, 1)
{
//std::string test = "BOxBMXiOxBMXiAAAABENDD6AAAAuuABgACBdYA";
std::string test = "BOOErvYOOErvZAAABAENAAAAAAAAqABgAAAAAA";
IabGdprUserConsentV1 consent;
IabGdprUserConsentSingleton::instance().insertAllGDPRConsents();
boost::dynamic_bitset<> dummy;
consent.setConsentStr(test.c_str(), dummy);

ASSERT_EQ(1, consent.m_GDPRVendorsConsent.size());
for (int  i = 1; i < 1; i++) {
    ASSERT_TRUE(consent.m_GDPRVendorsConsent[i]);
}
}
