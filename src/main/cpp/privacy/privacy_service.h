
#ifndef PRIVACY_SERVICE_H
#define PRIVACY_SERVICE_H

#include <boost/dynamic_bitset.hpp>
#include <mutex>
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "mem_buffer.h"
#include "base64_util.h"
#include "stat_counter.h"

namespace MM {
    namespace Privacy {


        enum ErrorType {
            noError,
            badSegmentType,
            errorInDecodingCoreString,
            errorInDecodingDisclosedVendors,
            errorInDecodingAllowedVendorsList,
        };

// 0  = Purpose Flatly Not Allowed by Publisher (regardless of Vendor declarations)
// 1  = Require Consent (if Vendor has declared the Purpose IDs legal basis as Legitimate Interest and flexible)
// 2  = Require Legitimate Interest (if Vendor has declared the Purpose IDs legal basis as Consent and flexible)
// 3  = UNDEFINED (not used)
        enum PubRestrictionType {
            not_allowed,
            requires_consent,
            requires_legitimate_interest,
            undefined
        };

        struct PrivacyRestrictionHash {
            template <typename T>
            std::size_t operator()(T t) const
            {
                return static_cast<std::size_t>(t);
            }
        };

        enum PrivacyRestriction {
            no_restrictions,

            no_precise_geo_location_usage,

            fully_restricted
        };

        typedef std::unordered_set<PrivacyRestriction, PrivacyRestrictionHash> RestrictionSetType;

        class Util {
        public:
            // populates a bitset from another bitset and moves the index
            // returns false in case of any failure
            // @param src - the bitset source that bits are copied from
            // @param start - the nth bit that copying is started from
            // @param size - the number of bits that get copied over
            // @param dest - the bitset destination that bits are copied to
            static bool populateBitsetAndMoveIndex(const boost::dynamic_bitset<>& src,
                                                   uint32_t& start,
                                                   const uint32_t size,
                                                   boost::dynamic_bitset<>& dest);
            // gets a long value from bitset
            // @param src - the bitset source that bits are read from
            // @param num_bits - the number of bits that are used to calculate the number
            // @param start - the first bit that is used to calculate the number from
            static long getLongAtPosition(const boost::dynamic_bitset<>& bitset, const uint32_t num_bits, const uint32_t start);

            // gets a long value from a bitset and moves the index aka, start
            // @param src - the bitset source that bits are read from
            // @param num_bits - the number of bits that are used to calculate the number
            // @param start - the first bit that is used to calculate the number from
            static long getLongMoveIndex(const boost::dynamic_bitset<>& bitset, const uint32_t num_bits, uint32_t& start);

            // returns the details of a privacy restrictions
            static std::vector<std::string> getPrivacyRestrictionInfo(const RestrictionSetType& restrictions);

            static std::string getPubRestrictionTypeName(const PubRestrictionType& type);

            static std::string getErrorTypeName(const ErrorType& type);
        };

        struct PubRestrictionEntry {
            // The Vendor’s declared Purpose ID that the publisher has indicated that they are overriding.
            uint32_t m_purposeId = 0;

            // 0  = Purpose Flatly Not Allowed by Publisher (regardless of Vendor declarations)
            // 1  = Require Consent (if Vendor has declared the Purpose IDs legal basis as Legitimate Interest and flexible)
            // 2  = Require Legitimate Interest (if Vendor has declared the Purpose IDs legal basis as Consent and flexible)
            // 3  = UNDEFINED (not used)
            PubRestrictionType m_restrictionType = undefined;

            std::unordered_set<uint32_t>* m_restrictedVendors = nullptr;

            PubRestrictionEntry();
            PubRestrictionEntry(PubRestrictionEntry const &) = delete;
            PubRestrictionEntry(PubRestrictionEntry &&) = delete;
            PubRestrictionEntry &operator=(const PubRestrictionEntry &other) = delete;
            PubRestrictionEntry &operator=(PubRestrictionEntry &&other) = delete;

            ~PubRestrictionEntry();
        };

// The detailed description of CoreString fields have been explained here
// https://github.com/InteractiveAdvertisingBureau/GDPR-Transparency-and-Consent-Framework/blob/master/TCFv2/IAB%20Tech%20Lab%20-%20Consent%20string%20and%20vendor%20list%20formats%20v2.md#the-core-string
        struct CoreStringData {
            static uint32_t BEGINNING_OF_CORE;
            static uint32_t VERSION_LENGTH;
            static uint32_t CREATE_LENGTH;
            static uint32_t UPDATED_LENGTH;
            static uint32_t CMP_ID_LENGTH;
            static uint32_t CMP_VERSION_LENGTH;
            static uint32_t CONSENT_SCREEN_LENGTH;
            static uint32_t CONSENT_LANGUAGE_LENGTH;
            static uint32_t VENDOR_LIST_VERSION_LENGTH;
            static uint32_t TCF_POLICY_VERSION_LENGTH;
            static uint32_t IS_SERVER_SPECIFIC_LENGTH;
            static uint32_t USE_NON_STANDARD_LENGTH;
            static uint32_t PURPOSE_CONTENT_LENGTH;
            static uint32_t PURPOSE_LI_TRANSPARENCY_LENGTH;
            static uint32_t SPECIAL_FEATURE_OPT_INS_LENGTH;
            static uint32_t PURPOSE_ON_TREATMENT_LENGTH;
            static uint32_t PUBLISHER_CC_LANGUAGE_LENGTH;

            uint32_t m_version = 0;
            long m_createdAt = 0;
            long m_lastUpdated = 0;
            uint32_t m_cmpId = 0;
            uint32_t m_cmpVersion = 0;
            uint32_t m_consentScreen = 0;
            std::string m_consentLanguage;
            uint32_t m_vendorListVersion = 0;
            uint32_t m_tcfPolicyVersion = 0;
            bool m_isServiceSpecific = false;
            bool m_useNonStandardStacks = false;

            // 12 bits, One bit for each Special Feature: 1 Opted in 0 Not opted in
            boost::dynamic_bitset<> m_specialFeatureOptIns = boost::dynamic_bitset<>(SPECIAL_FEATURE_OPT_INS_LENGTH);

            // 24 bits, One bit for each Purpose: 1 Consent 0 No Consent
            // The user’s consent value for each Purpose established on the legal basis of consent.
            // The Purposes are numerically identified and published in the Global Vendor List.
            // From left to right, Purpose 1 maps to the 0th bit, purpose 24 maps to the bit at index 23.
            // Special Purposes are a different ID space and not included in this field.
            boost::dynamic_bitset<> m_purposesConsent = boost::dynamic_bitset<>(PURPOSE_CONTENT_LENGTH);

            // 24 bits, One bit for each Purpose: 1 legitimate interest established 0 legitimate interest was NOT established or
            // it was established but user exercised their “Right to Object” to the Purpose	The Purpose’s transparency
            // requirements are met for each Purpose on the legal basis of legitimate interest
            // and the user has not exercised their “Right to Object” to that Purpose.
            // By default or if the user has exercised their “Right to Object” to a Purpose,
            // the corresponding bit for that Purpose is set to 0.
            // From left to right, Purpose 1 maps to the 0th bit, purpose 24 maps to the bit at index 23.
            // Special Purposes are a different ID space and not included in this field.
            boost::dynamic_bitset<> m_purposesLITransparency = boost::dynamic_bitset<>(PURPOSE_LI_TRANSPARENCY_LENGTH);

            // Specific Jurisdiction Disclosures
            bool m_purposeOneTreatment = false;
            std::string m_publisherCC;

            // Vendor Consent Section
            std::unordered_map<uint32_t, bool> m_vendorConsents;

            // Vendor Legitimate Interest Section
            std::unordered_map<uint32_t, bool> m_vendorInterests;

            std::unordered_map<std::pair<uint32_t, uint32_t>, PubRestrictionEntry*, boost::hash<std::pair<uint32_t, uint32_t>>>* purposeIdToPublisherRestriction = nullptr;

            CoreStringData();
            CoreStringData(CoreStringData const &) = delete;
            CoreStringData(CoreStringData &&) = delete;
            CoreStringData &operator=(const CoreStringData &other) = delete;
            CoreStringData &operator=(CoreStringData &&other) = delete;

            ~CoreStringData();
        };

// TCFV2Context has a specific format that is explained here
// https://github.com/InteractiveAdvertisingBureau/GDPR-Transparency-and-Consent-Framework/blob/master/TCFv2/IAB%20Tech%20Lab%20-%20Consent%20string%20and%20vendor%20list%20formats%20v2.md#tc-string-format
        class TCFV2Context {
            static std::string m_mapDivStyle;
            friend class PrivacyService;
        public:
            ErrorType errorCode = noError;
            RestrictionSetType privacyRestrictions;
            std::string m_consentStringBase64;
            CoreStringData m_coreStringData;

            // The DisclosedVendors is a TC String segment that signals
            // which vendors have been disclosed to a given user by a CMP.
            std::unordered_map<uint32_t, bool>* m_disclosedVendorListInfo = nullptr;

            std::unordered_map<uint32_t, bool>* m_allowedVendorListInfo = nullptr;

            TCFV2Context();
            TCFV2Context(TCFV2Context const &) = delete;
            TCFV2Context(TCFV2Context &&) = delete;
            TCFV2Context &operator=(const TCFV2Context &other) = delete;
            TCFV2Context &operator=(TCFV2Context &&other) = delete;

            uint32_t getVersion() const;

            void report(MM::Utils::MemBuffer& out) const;

            void reportMap(MM::Utils::MemBuffer& out, const std::unordered_map<uint32_t, bool>& map) const;

            ~TCFV2Context();
        };

        class TCFV2Decoder {

        private:
            static uint32_t MAX_VENDOR_ID_LENGTH;
            static uint32_t IS_RANGE_ENCODING_LENGTH;
            static uint32_t NUMBER_OF_ENTRIES_LENGTH;
            static uint32_t IS_RANGE_LENGTH;
            static uint32_t START_OR_ONLY_VENDOR_ID_LENGTH;
            static uint32_t NUMB_PUB_RESTRICTIONS_LENGTH;
            static uint32_t PUB_RESTRICTION_TYPE_LENGTH;
            static uint32_t SEGMENT_TYPE_LENGTH;
            static uint32_t PURPOSE_ID_LENGTH;

            static void parseOptionalParts(const boost::dynamic_bitset<>& allBitSet, TCFV2Context& context, uint32_t& readAtPos);

            static bool parsePublisherRestrictionSection(const boost::dynamic_bitset<>& allBitSet,
                                                         uint32_t& readAtPos, CoreStringData& data);

            static bool parseCoreStringGeneralSection(const boost::dynamic_bitset<>& allBitSet,
                                                      uint32_t& readAtPos, CoreStringData& data);

            static bool parseSectionWithBitFieldOrRange(const boost::dynamic_bitset<>& allBitSet, uint32_t& readAtPos,
                                                        std::unordered_map<uint32_t, bool>& consentMap);

            static bool parseCoreStringData(const boost::dynamic_bitset<>& allBitSet,
                                            uint32_t& readAtPos, CoreStringData& data);

            TCFV2Decoder();

        public:
            static void parseConsentString(TCFV2Context& context, const std::string& consentString, const bool& decodeOptionalParts = false);
        };

        class PrivacyService {

        private:
            static PrivacyService* m_inst;
            static std::mutex s_mutex;

            mutable MM::Utils::StatCounter m_mmAllowedToBidCntr;
            mutable MM::Utils::StatCounter m_mmNotAllowedToBidCntr;
            mutable MM::Utils::StatCounter m_vendorSyncAllowedCntr;
            mutable MM::Utils::StatCounter m_vendorSyncNotAllowedCntr;
            mutable MM::Utils::StatCounter m_noRestrictionsCntr;
            mutable MM::Utils::StatCounter m_noPreciseGeolocationUsageCntr;
            mutable MM::Utils::StatCounter m_fullyRestrictedCntr;
            mutable MM::Utils::StatCounter m_blockeByPubRestrictionCntr;
            mutable MM::Utils::StatCounter m_contextHasErrorCntr;

            std::vector<MM::Utils::StatCounter*> m_allV2Cntrs;
            std::vector<MM::Utils::StatCounter*> m_OverallCntrs;

        public:
            bool isVendorFreeOfRequirement(const uint32_t& vendorId, const std::initializer_list<uint32_t> &purposeIds, PubRestrictionType restrictionType,
                                           const std::unordered_map<std::pair<uint32_t, uint32_t>, PubRestrictionEntry*, boost::hash<std::pair<uint32_t, uint32_t>>>& purposeIdToPublisherRestriction) const;

            std::vector<MM::Utils::StatCounter*> getAllV2Cntrs() const;
            std::vector<MM::Utils::StatCounter*>* getOverallCntrs();

            void getDecisionOnMalformedConsentString(TCFV2Context& context);
            void getBiddingPrivacyRestrictions(const uint32_t& vendorId, TCFV2Context& context);
            void getBiddingPrivacyRestrictionsInTransitionPeriod(const uint32_t& vendorId, TCFV2Context& context);
            void getBiddingPrivacyRestrictionsInPostTransitionPeriod(const uint32_t& vendorId, TCFV2Context& context);

            bool isVendorAllowedToBid(const uint32_t gvId, TCFV2Context& context);
            bool isVendorUserSyncAllowed(const uint32_t& vendorId, TCFV2Context& context);

            PrivacyService();

            static PrivacyService* instance();

            PrivacyService(PrivacyService const &) = delete;
            PrivacyService(PrivacyService &&) = delete;
            PrivacyService &operator=(const PrivacyService &other) = delete;
            PrivacyService &operator=(PrivacyService &&other) = delete;

            static uint32_t getVersion(const std::string& consentStr);
            void countRestrictions(const RestrictionSetType& restricitons);
        };

    } // namespace Privacy
} // namespace MM

#endif
