
#ifndef PRIVACY_SERVICE_GDPR_TCFV2_TimeString_H
#define PRIVACY_SERVICE_GDPR_TCFV2_TimeString_H

// @version: $Id: time_string.h 1 2009-12-09 19:05:10Z vli $

#include <ctime>
#include <string>

// A utility class provides various time formating (for HTML, XML, email)

namespace MM {
    namespace Utils {

        class TimeString
        {
        public:
            ///
            /// for HTML
            const char* gmTimeForHTML();
            const char* gmTimeForHTML(time_t ltime);

            /// convert an HTTP date-time string into time_t.
            /// return 0 if input is not a valid datetime string
            static time_t httpDateToUnixTime(const char* httpDate);

            ///
            /// for regular message (YYYY-MM-DD HH24:MI:SS)
            const char* gmTimeForMsgLog();
            const char* gmTimeForMsgLog(time_t ltime);

            const char* localTimeForMsgLog();
            const char* localTimeForMsgLog(time_t ltime);

            /// just the date part, YYYY-MM-DD
            const char* gmDateForMsgLog();
            const char* gmDateForMsgLog(time_t ltime);

            ///
            /// for unix/linux filenames (YYYYMMDD_HH24MISS)
            const char* gmTimeForFilename();
            const char* gmTimeForFilename(time_t ltime);

            const char* localTimeForFilename();
            const char* localTimeForFilename(time_t ltime);

            ///
            /// for xsd:dateTime
            const char* gmDateTimeForXML();
            const char* gmDateTimeForXML(time_t ltime);

            const char* localDateTimeForXML();
            const char* localDateTimeForXML(time_t ltime);

            ///
            /// for xsd:time
            const char* gmTimeForXML();
            const char* gmTimeForXML(time_t ltime);

            const char* localTimeForXML();
            const char* localTimeForXML(time_t ltime);

            ///
            /// for xsd:date
            const char* localDateForXML();
            const char* localDateForXML(time_t ltime);

            ///
            /// translate time into days, mins, sec.
            const char* humanTime(int secs);

            static time_t extractTimestampFromFile(const std::string& file);

            ///
            ///
//	const char* localTimeForEmail();
//	const char* localTimeForEmail(time_t ltime);

        private:
            char m_buffer[128];
        };

//
// inline functions
//
        inline
        const char* TimeString::gmTimeForHTML() {
            return gmTimeForHTML(time(0));
        }

        inline
        const char* TimeString::gmTimeForMsgLog() {
            return gmTimeForMsgLog(time(0));
        }
        inline
        const char* TimeString::localTimeForMsgLog() {
            return localTimeForMsgLog(time(0));
        }

        inline
        const char* TimeString::gmDateForMsgLog() {
            return gmDateForMsgLog(time(0));
        }

        inline
        const char* TimeString::gmTimeForFilename() {
            return gmTimeForFilename(time(0));
        }
        inline
        const char* TimeString::localTimeForFilename() {
            return localTimeForFilename(time(0));
        }

        inline
        const char* TimeString::gmDateTimeForXML() {
            return gmDateTimeForXML(time(0));
        }

        inline
        const char* TimeString::gmTimeForXML() {
            return gmTimeForXML(time(0));
        }

        inline
        const char* TimeString::localDateTimeForXML() {
            return localDateTimeForXML(time(0));
        }

        inline
        const char* TimeString::localTimeForXML() {
            return localTimeForXML(time(0));
        }

        inline
        const char* TimeString::localDateForXML() {
            return localDateForXML(time(0));
        }

    }
}

#endif