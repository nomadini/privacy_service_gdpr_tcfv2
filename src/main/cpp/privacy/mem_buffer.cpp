#include <inttypes.h>
#include "mem_buffer.h"
#include "smart_buffer.h"
#include <ios>
#include <strstream>
#include <iomanip>


using namespace MM::Utils;

MemBuffer::MemBuffer() :
        m_nUsedSize(0),
        m_nGrowSize(e_def_grow_size),
        m_defPrecision(3)
{
    m_pData = m_internalBuf;
    m_nBufSize = sizeof(m_internalBuf);
    m_bOwnTheBuffer = false;
    m_pCurAt = m_pData;
    if (0 != m_pCurAt)
        *m_pCurAt = '\0';
}

MemBuffer::MemBuffer(const MemBuffer& mb) :
        m_nUsedSize(mb.m_nUsedSize),
        m_nGrowSize(mb.m_nGrowSize),
        m_bOwnTheBuffer(true),
        m_defPrecision(3)
{
    if (mb.m_nBufSize <= sizeof(m_internalBuf)) {
        m_pData = m_internalBuf;
        m_nBufSize = sizeof(m_internalBuf);
        m_bOwnTheBuffer = false;
    }
    else {
        m_pData = new char[mb.m_nBufSize];
        m_nBufSize = mb.m_nBufSize;
    }

    m_pCurAt = m_pData;
    *m_pCurAt = '\0';
    if (m_nUsedSize) {
        memcpy(m_pData, mb.m_pData, m_nUsedSize);
        m_pCurAt = m_pData + m_nUsedSize;
        if (m_nUsedSize < m_nBufSize)   // this should always be true
            *m_pCurAt = '\0';
    }
}

MemBuffer::MemBuffer(unsigned int nInitSize, unsigned int nGrowSize) :
        m_nUsedSize(0),
        m_bOwnTheBuffer(true),
        m_defPrecision(3)
{
    m_nGrowSize = (nGrowSize > 0) ? nGrowSize : e_def_grow_size;

    if (nInitSize <= sizeof(m_internalBuf)) {
        m_pData = m_internalBuf;
        m_nBufSize = sizeof(m_internalBuf);
        m_bOwnTheBuffer = false;
    }
    else {
        m_pData = new char[nInitSize];
        m_nBufSize = nInitSize;
        m_bOwnTheBuffer = true;
    }
    m_pCurAt = m_pData;
    if (0 != m_pCurAt)
        *m_pCurAt = '\0';
}

MemBuffer::MemBuffer(char* pExtBuffer, unsigned int nExtBufSize, unsigned int nGrowSize) :
        m_pData(pExtBuffer),
        m_nBufSize(nExtBufSize),
        m_nUsedSize(0),
        m_bOwnTheBuffer(false),
        m_defPrecision(3)
{
    m_nGrowSize = (nGrowSize > 0) ? nGrowSize : e_def_grow_size;
    m_pCurAt = m_pData;
    if (0 != m_pCurAt)
        *m_pCurAt = '\0';
}


MemBuffer::~MemBuffer()
{
    if (m_bOwnTheBuffer)
        delete[] m_pData;
}

MemBuffer::MemBuffer(MemBuffer&& mb) :
        m_pData(NULL),
        m_pCurAt(m_pData),
        m_nBufSize(0),
        m_nUsedSize(0),
        m_nGrowSize(e_def_grow_size),
        m_bOwnTheBuffer(true),
        m_defPrecision(3)
{
    swap(mb);
}

void MemBuffer::swap(MemBuffer& mb)
{
    if (&mb != this) {
        if (!m_bOwnTheBuffer) {
            // if i don't own the buffer, i have to make a copy of the data
            // in order to proceed the swap
            if (m_nBufSize) {
                char* data = new char[m_nBufSize];
                memcpy(data, m_pData, (m_nUsedSize < m_nBufSize) ? (m_nUsedSize + 1) : m_nUsedSize);
                m_pData = data;
            }
            m_bOwnTheBuffer = true;
        }
        if (!mb.m_bOwnTheBuffer) {
            if (mb.m_nBufSize) {
                char* data = new char[mb.m_nBufSize];
                memcpy(data, mb.m_pData, (mb.m_nUsedSize < mb.m_nBufSize) ? (mb.m_nUsedSize + 1) : mb.m_nUsedSize);
                mb.m_pData = data;
            }
            mb.m_bOwnTheBuffer = true;
        }

        char* tempP = m_pData;
        m_pData = mb.m_pData;
        mb.m_pData = tempP;
        //
        tempP = m_pCurAt;
        m_pCurAt = mb.m_pCurAt;
        mb.m_pCurAt = tempP;

        //
        auto tempI = m_nBufSize;
        m_nBufSize = mb.m_nBufSize;
        mb.m_nBufSize = tempI;
        //
        tempI = m_nUsedSize;
        m_nUsedSize = mb.m_nUsedSize;
        mb.m_nUsedSize = tempI;
        //
        tempI = m_nGrowSize;
        m_nGrowSize = mb.m_nGrowSize;
        mb.m_nGrowSize = tempI;
        //
        tempI = m_defPrecision;
        m_defPrecision = mb.m_defPrecision;
        mb.m_defPrecision = tempI;
    }
}

void MemBuffer::reserve(size_t size)
{
    if (size > m_nUsedSize)
        growIfNeeded(size - m_nUsedSize);
}

void MemBuffer::resize(size_t size)
{
    if (size >= capacity()) {
        reserve(size);
    }
    m_nUsedSize = size;
    m_pCurAt = m_pData + size;
    *m_pCurAt = '\0';
}

MemBuffer& MemBuffer::operator=(const MemBuffer& mb)
{
    if (&mb != this) {
        if (m_bOwnTheBuffer)
            delete[] m_pData;

        if (mb.m_nUsedSize < sizeof(m_internalBuf)) {
            m_nBufSize      = sizeof(m_internalBuf);
            m_pData         = m_internalBuf;
            m_bOwnTheBuffer = false;
        }
        else {
            m_nBufSize      = mb.m_nBufSize;
            m_pData         = new char[m_nBufSize];
            m_bOwnTheBuffer = true;
        }

        m_nUsedSize = mb.m_nUsedSize;
        m_nGrowSize = mb.m_nGrowSize;

        if (m_nUsedSize) {
            memcpy(m_pData, mb.m_pData, m_nUsedSize);
        }
        m_pCurAt = m_pData + m_nUsedSize;
        *m_pCurAt = '\0';
    }
    return *this;
}

MemBuffer& MemBuffer::operator=(MemBuffer&& mb)
{
    if (&mb != this) {
        if (m_bOwnTheBuffer && m_pData)
            delete[] m_pData;
        m_pData = nullptr;
        m_pCurAt = m_pData;
        m_nBufSize = 0;
        m_nUsedSize = 0;
        m_bOwnTheBuffer = true;

        swap(mb);
    }
    return *this;
}

MemBuffer& MemBuffer::operator=(const char* pStr)
{
    size_t len = pStr ? strlen(pStr) : 0;
    if (len > 0) {
        if ((pStr + len <  m_pData) ||
            (pStr >= m_pData + m_nBufSize))
        {
            clear();
            append(pStr, len);
        }
        else if (pStr != m_pData) {
            std::string szTemp(pStr);
            clear();
            append(szTemp.c_str(), szTemp.length());
        }
    }
    else
        clear();

    return *this;
}

MemBuffer& MemBuffer::operator=(const std::string& szStr)
{
    clear();
    if (! szStr.empty())
        append(szStr.c_str(), szStr.length());
    return *this;
}

MemBuffer& MemBuffer::append(const void* pData, size_t nDataLen)
{
    if (nDataLen) {
        growIfNeeded(nDataLen);

        if (nDataLen > 1) {
            memcpy(m_pCurAt, pData, nDataLen);
            m_pCurAt += nDataLen;
            m_nUsedSize += nDataLen;
        }
        else {
            *m_pCurAt++ = *((char*)pData);
            m_nUsedSize++;
        }
        *m_pCurAt = '\0';
    }
    return *this;
}

MemBuffer& MemBuffer::append(int16_t number)
{
    growIfNeeded(MAX_LEN_INT16);
    int len = to_str(static_cast<int32_t>(number), m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}

MemBuffer& MemBuffer::append(uint16_t number)
{
    growIfNeeded(MAX_LEN_INT16);
    int len = to_str(static_cast<uint32_t>(number), m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}

MemBuffer& MemBuffer::append(int32_t number)
{
    growIfNeeded(MAX_LEN_INT32);
    int len = to_str(number, m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}

MemBuffer& MemBuffer::append(uint32_t number)
{
    growIfNeeded(MAX_LEN_INT32);
    int len = to_str(number, m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}

MemBuffer& MemBuffer::append(int64_t number)
{
    growIfNeeded(MAX_LEN_INT64);
    int len = to_str(number, m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}

MemBuffer& MemBuffer::append(uint64_t number)
{
    growIfNeeded(MAX_LEN_INT64);
    int len = to_str(number, m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}

MemBuffer& MemBuffer::append(__int128 number)
{
    growIfNeeded(MAX_LEN_INT128);
    int len = to_str(number, m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}

MemBuffer& MemBuffer::append(unsigned __int128 number)
{
    growIfNeeded(MAX_LEN_INT128);
    int len = to_str(number, m_pCurAt);
    m_pCurAt += len;
    m_nUsedSize += len;
    *m_pCurAt = '\0';
    return *this;
}



MemBuffer& MemBuffer::append(float number, int precision)
{
    if (0 == precision)
        precision = m_defPrecision;
    char buf[64];
    std::strstream text(buf, sizeof(buf));
    text.setf(std::ios::fixed);
    text << std::setprecision(precision) << number;
    return append(buf, (size_t)text.pcount());
}

MemBuffer& MemBuffer::append(double number, int precision)
{
    if (0 == precision)
        precision = m_defPrecision;
    char buf[64];
    std::strstream text(buf, sizeof(buf));
    text.setf(std::ios::fixed);
    text << std::setprecision(precision) << number;
    return append(buf, (size_t)text.pcount());
}

MemBuffer& MemBuffer::appendHex(uint8_t number)
{
    char buf[16];
    int l = snprintf(buf, sizeof(buf), "0x%02hhX", number);
    return append(buf, l);
}

MemBuffer& MemBuffer::appendHex(uint16_t number)
{
    char buf[16];
    int l = snprintf(buf, sizeof(buf), "0x%04hX", number);
    return append(buf, l);
}

MemBuffer& MemBuffer::appendHex(uint32_t number)
{
    char buf[16];
    int l = snprintf(buf, sizeof(buf), "0x%08X", number);
    return append(buf, l);
}

MemBuffer& MemBuffer::appendHex(uint64_t number)
{
    char buf[24];
    int l = snprintf(buf, sizeof(buf), "0x%016lX", number);
    return append(buf, l);
}

MemBuffer& MemBuffer::appendHex(const std::string& str)
{
    int len = 10 + 2 * str.size();
    SmartBuffer buf(len + 1);
    char* p = buf;
    *p = (char)'0';
    ++p;
    *p = (char)'x';
    ++p;
    len -= 2;
    for (size_t i = 0; i < str.size(); i++) {
        unsigned byte = str.at(i);
        int l = snprintf(p, len, "%02hhX", byte);
        p += l;
        len -= l;
        if (len <= 0)
            break;
    }
    return append((const char*)buf);
}

void MemBuffer::growIfNeeded(size_t nReqSize)
{
    // minus 1 for the null-terminator
    if (m_nUsedSize + nReqSize + 1 <= m_nBufSize)
        return;

    unsigned int nNS = 0;
    if (!m_nBufSize && e_def_init_size > nReqSize) {
        // first time use, and the default size is good enough
        nNS = e_def_init_size;
    }
    else {
        unsigned int nExtraSize = (m_nUsedSize + nReqSize) - m_nBufSize + 1;
        nExtraSize = (nExtraSize + m_nGrowSize-1) / m_nGrowSize * m_nGrowSize;
        nNS = m_nBufSize + nExtraSize;
    }

    char* pNewBuf = new char[nNS];
    if (!pNewBuf) {
        nNS = m_nBufSize + nReqSize + 1;
        pNewBuf = new char[nNS];
        // if this allocation fails again, we're doomed!
    }
    m_nBufSize = nNS;
    if (m_pData) {
        memcpy(pNewBuf, m_pData, m_nUsedSize);
        if (m_bOwnTheBuffer)
            delete[] m_pData;
        else
            m_bOwnTheBuffer = true;
    }
    m_pData = pNewBuf;
    m_pCurAt = m_pData + m_nUsedSize;
}

MemBuffer& MemBuffer::insert(iterator pos, const char* s, size_t n)
{
    if (pos == m_pCurAt) {
        append(s, n);
    }
    else if ((pos >= m_pData) && (pos < m_pData + m_nUsedSize)) {
        growIfNeeded(n);
        memmove(pos+n, pos, m_pData + m_nUsedSize - pos + 1);
        memcpy(pos, s, n);
        m_nUsedSize += n;
        m_pCurAt = end();
    }
    return *this;
}

MemBuffer& MemBuffer::insert(iterator pos, const char* first, const char* last)
{
    return insert(pos, first, last - first);
}

void MemBuffer::reverseString(char* begin, char* end)
{
    char tmp;
    while (end > begin) {
        tmp = *end, *end-- = *begin, *begin++ = tmp;
    }
}

int MemBuffer::to_str(int32_t v, char* buf)
{
    char* begin = buf;
    if (v < 0) {
        v = -v;
        *begin++ = '-';
    }
    char* end = begin;
    do {
        *end++ = (char)('0' + (v % 10));
    } while (v /= 10);
    *end = '\0';

    reverseString(begin, end-1);
    return end - buf;
}

int MemBuffer::to_str(uint32_t u, char* buf)
{
    char* begin = buf;
    char* end = buf;
    do {
        *end++ = (char)('0' + (u % 10));
    } while (u /= 10);
    *end = '\0';

    reverseString(begin, end-1);
    return end - buf;
}

int MemBuffer::to_str(int64_t v, char* buf)
{
    char* begin = buf;
    if (v < 0) {
        v = -v;
        *begin++ = '-';
    }
    char* end = begin;
    do {
        *end++ = (char)('0' + (v % 10));
    } while (v /= 10);
    *end = '\0';

    reverseString(begin, end-1);
    return end - buf;
}

int MemBuffer::to_str(uint64_t u, char* buf)
{
    char* begin = buf;
    char* end = buf;
    do {
        *end++ = (char)('0' + (u % 10));
    } while (u /= 10);
    *end = '\0';

    reverseString(begin, end-1);
    return end - buf;
}

int MemBuffer::to_str(__int128 v, char* buf_41)
{
    char* begin = buf_41;
    if (v < 0) {
        v = -v;
        *begin++ = '-';
    }
    char* end = begin;
    do {
        *end++ = (char)('0' + (v % 10));
    } while (v /= 10);
    *end = '\0';

    reverseString(begin, end-1);
    return end - buf_41;
}

int MemBuffer::to_str(unsigned __int128 u, char* buf_41)
{
    char* begin = buf_41;
    char* end = buf_41;
    do {
        *end++ = (char)('0' + (u % 10));
    } while (u /= 10);
    *end = '\0';

    reverseString(begin, end-1);
    return end - buf_41;
}
