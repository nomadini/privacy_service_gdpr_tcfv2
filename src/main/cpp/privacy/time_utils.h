#pragma once

// @version: $Id: time_utils.h 1 2009-12-09 19:05:10Z vli $

#include <ctime>

namespace MM {

namespace Utils {

// return milliseconds since an arbitrary point in the past (since the machine started).
unsigned long getMilliSecond();

// a fast, very cheap c-runtime "time(0)" replacement.
// if millisec != 0, store the milli-second part in the pointer (accuracy is low though :P)
time_t fast_time_t(int* millisec = 0);

// returns the time_t value to the beginning of the hour wrt to now
time_t to_hour_start(time_t now);

// returns the time_t value to the beginning of the gmt day wrt to now
time_t to_gmt_day_start(time_t now);

// returns the time_t value to the beginning of the day wrt to now
time_t to_day_start(time_t now);

// returns the time_t value to the beginning of the week wrt to now where start of week is Sunday.
time_t to_week_start(time_t now);

// returns the time_t value to the beginning of the month wrt to now
time_t to_month_start(time_t now);

// returns the day count in this year for now
int day_in_year(time_t now);

// return true if tm1 and tm2 in the same gmt day
 bool same_day(time_t tm1, time_t tm2);

}
}
