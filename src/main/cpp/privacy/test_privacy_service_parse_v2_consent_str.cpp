//
// Created by Mahmoud Taabodi on 3/4/20.
//

#include "gtest/gtest.h"
#include "privacy_service.h"
#include "iabGdprUserConsentV1.h"
#include "privacy_test_util.h"
using namespace MM::Privacy;

TEST(GDPR_CORE_STRING_Short, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvsZChOvsZChAIAAAENAPCAAAAAAAAAAAAAAAAAAAAA";

TCFV2Decoder::parseConsentString(context, consentString);

ASSERT_EQ(2, context.m_coreStringData.m_version);
ASSERT_EQ(15832551585, context.m_coreStringData.m_createdAt);
ASSERT_EQ(15832551585, context.m_coreStringData.m_lastUpdated);
ASSERT_EQ(8, context.m_coreStringData.m_cmpId);
ASSERT_EQ(0, context.m_coreStringData.m_cmpVersion);
ASSERT_EQ(0, context.m_coreStringData.m_consentScreen);
ASSERT_EQ("EN", context.m_coreStringData.m_consentLanguage);
ASSERT_EQ(15, context.m_coreStringData.m_vendorListVersion);
ASSERT_EQ(2, context.m_coreStringData.m_tcfPolicyVersion);
ASSERT_FALSE(context.m_coreStringData.m_isServiceSpecific);
ASSERT_FALSE(context.m_coreStringData.m_useNonStandardStacks);
ASSERT_FALSE(context.m_coreStringData.m_purposeOneTreatment);
ASSERT_EQ("AA", context.m_coreStringData.m_publisherCC);
}

TEST(GDPR_CORE_STRING_WITH_LEGITIMATE_INTEREST, PARSE_CORE_STRING)
{
TCFV2Context context;
//with 37 legitimate interest
std::string consentString = "COvwl0NOvwl0NMYAAAENAPCAAAAAAAAAAAAAAAAAlAAghAAQAA";
TCFV2Decoder::parseConsentString(context, consentString);

ASSERT_EQ(2, context.m_coreStringData.m_version);
ASSERT_EQ(792, context.m_coreStringData.m_cmpId);
PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests,
4, {12, 18, 23, 37});
}

TEST(GDPR_CORE_STRING_WITH_VENDOR_DISCLOSED, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvwH4gOvwH4gNlAAAENAPCAAAAAAAAAAAAAAAAAAAAA."
                            "IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAg"
                            "AAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
TCFV2Decoder::parseConsentString(context, consentString);

ASSERT_EQ(2, context.m_coreStringData.m_version);
ASSERT_EQ(15833529888, context.m_coreStringData.m_createdAt);
ASSERT_EQ(15833529888, context.m_coreStringData.m_lastUpdated);
ASSERT_EQ(869, context.m_coreStringData.m_cmpId);
ASSERT_EQ(0, context.m_coreStringData.m_cmpVersion);
ASSERT_EQ(0, context.m_coreStringData.m_consentScreen);
ASSERT_EQ("EN", context.m_coreStringData.m_consentLanguage);
ASSERT_EQ(15, context.m_coreStringData.m_vendorListVersion);
ASSERT_EQ(2, context.m_coreStringData.m_tcfPolicyVersion);
ASSERT_FALSE(context.m_coreStringData.m_isServiceSpecific);
ASSERT_FALSE(context.m_coreStringData.m_useNonStandardStacks);
ASSERT_FALSE(context.m_coreStringData.m_purposeOneTreatment);
ASSERT_FALSE(context.m_coreStringData.m_purposeOneTreatment);
ASSERT_EQ("AA", context.m_coreStringData.m_publisherCC);
}

TEST(PARSE_VENDOR_ALLOWED, PARSE_VENDORS_STRING)
{
IabGdprUserConsentSingleton::instance().decodeOptionalParts = true;
TCFV2Context context;
std::string consentString = "COvyzPqOvyzPqIEAAAENAPCAAAAAAAAAAAAAAAAAAAAA."
                            "IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgA"
                            "QBAAAAAAAGBAAgAAAAAAAFAAECAAA"
                            "gAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw.QAGAUQ";
TCFV2Decoder::parseConsentString(context, consentString);

//this case has 79 true vendors which we only verify 5 of them
PrivacyTestUtil::verifyMapContent(*context.m_disclosedVendorListInfo, 79, {2, 6, 8, 719, 720});
PrivacyTestUtil::verifyMapContent(*context.m_allowedVendorListInfo, 3, {6, 8, 12});
}

TEST(VERIFY_VENDOR_CONSENT_BIT_FIELD_CASE, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvw4gMOvw4gMGQAAAENAPCAAAAAAAAAAAAAADAQAAAAAA."
                            "IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAg"
                            "AAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 1, {6});
}

TEST(VERIFY_VENDOR_CONSENT_RANGE_WITH_NO_END_CASE, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvxBEVOvxBEVEmAAAENAPCAAAAAAAAAAAAAFoQAQFoAAAAA";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 1, {720});
}

TEST(VERIFY_VENDOR_CONSENT_BIT_FIELD_NO_2, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvx4kSOvx4kSKbAAAENAPCAAAAAAAAAAAAAAJAAQQAAAAA";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 2, {12, 18});
}

TEST(VERIFY_VENDOR_CONSENT_BIT_FIELD_NO_3_NON_WEB_SAFE, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvx5D_Ovx5D_PYAAAENAPCAAAAAAAAAAAAADzQCAADAAIABKACoAr4BZgDpgHmAAAAA";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 8,
{6, 8, 37, 42, 351, 358, 467, 486});
}

TEST(VERIFY_VENDOR_CONSENT_RANGE_WITH_NO_END_CASE_NO_2, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvxDXdOvxDXdJuAAAENAPCAAAAAAAAAAAAADTQAgAEAGmAAAAAA";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 2, {8, 422});
}

TEST(VERIFY_VENDOR_CONSENT_RANGE_WITH_NO_END_CASE_MULTIPLE_RANGE_ENTRIES, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvx8E3Ovx8E3KaAAAENAPCAAAAAAAAAAAAAD5QCAADAAIAAYABIA34B0wDzAHyAAAAA";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 8,
{6, 8, 12, 18, 447, 467, 486, 498});
}

TEST(VERIFY_VENDOR_LEGITIMATE_INTEREST_RANGE_WITH_NO_END_CASE_MULTIPLE_RANGE_ENTRIES_CASE1, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvyCnsOvyCnsEKAAAENAPCAAAAAAAAAAAAAAAALPgBALMAWeAAA";
TCFV2Decoder::parseConsentString(context, consentString);
ASSERT_EQ(0, context.m_coreStringData.m_vendorConsents.size());
ASSERT_EQ(2, context.m_coreStringData.m_vendorInterests.size());

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests, 2, {716, 719});
}

TEST(VERIFY_VENDOR_LEGITIMATE_INTEREST_RANGE_WITH_NO_END_CASE_MULTIPLE_RANGE_ENTRIES_CASE2, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvyB_YOvyB_YNUAAAENAPCAAAAAAAAAAAAAAAAAIAoAAA";
TCFV2Decoder::parseConsentString(context, consentString);
ASSERT_EQ(0, context.m_coreStringData.m_vendorConsents.size());

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests, 2, {6, 8});
}

TEST(VERIFY_VENDOR_LEGITIMATE_INTEREST_RANGE_WITH_NO_END_CASE_MULTIPLE_RANGE_ENTRIES_CASE3, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvyCRvOvyCRvDiAAAENAPCAAAAAAAAAAAAAAAAAlAIghAAQAA";
TCFV2Decoder::parseConsentString(context, consentString);
ASSERT_EQ(0, context.m_coreStringData.m_vendorConsents.size());

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests,
5, {8, 12, 18, 23, 37});
}

TEST(VERIFY_VENDOR_LEGITIMATE_INTEREST_RANGE_WITH_NO_END_CASE_MULTIPLE_RANGE_ENTRIES_CASE4, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvyDC8OvyDC8FNAAAENAPCAAAAAAAAAAAAAAAALMgBgLIAWUAswAAA";
TCFV2Decoder::parseConsentString(context, consentString);
ASSERT_EQ(0, context.m_coreStringData.m_vendorConsents.size());

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests, 3,
{712, 714, 716});
}

TEST(VERIFY_VENDOR_LEGITIMATE_INTEREST_AND_VENDOR_CONSENT_CASE1, PARSE_CORE_STRING)
{

TCFV2Context context;
std::string consentString = "COvyDXxOvyDXxJIAAAENAPCAAAAAAAAAAAAAASgAAAgAIAJAAQQAAA";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests, 2, {12, 18});
PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 2, {23, 37});
}

TEST(VERIFY_PARSING_MOST_FIELDS, PARSE_CORE_STRING)
{
IabGdprUserConsentSingleton::instance().decodeOptionalParts = true;

TCFV2Context context;
std::string consentString = "COv1UGJOv1UGJL6AAAENAPCMAOAAAOAAAAAAAEEUACCKAAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw.QAEEUA";
TCFV2Decoder::parseConsentString(context, consentString);

PrivacyTestUtil::verifyBitsetContent(context.m_coreStringData.m_purposesConsent, 24, {0, 1, 2});
PrivacyTestUtil::verifyBitsetContent(context.m_coreStringData.m_purposesLITransparency, 24, {0, 1, 2});
PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 3, {2, 6, 8});
PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests, 3, {2, 6, 8});
PrivacyTestUtil::verifyMapContent(*context.m_allowedVendorListInfo, 3, {2, 6, 8});
PrivacyTestUtil::verifyMapContent(*context.m_disclosedVendorListInfo, 79, {2, 6, 8, 719, 720});

ASSERT_EQ(12, context.m_coreStringData.m_specialFeatureOptIns.size());
}

TEST(VERIFY_PARSING_MULTI_RANGE_CASE_IN_VENDOR_CONSENTS, PARSE_STRING)
{
IabGdprUserConsentSingleton::instance().decodeOptionalParts = true;
TCFV2Context context;
std::string consentString = "COwKkKsOwKkKsO9AAAENAPCAAH4AAD4AAAAAFmQA4FXgVhAsMCxIFjgWQAFQBEEIACEAAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
TCFV2Decoder::parseConsentString(context, consentString);

ASSERT_EQ(2, context.m_coreStringData.m_version);
ASSERT_EQ(15840461484, context.m_coreStringData.m_createdAt);
ASSERT_EQ(15840461484, context.m_coreStringData.m_lastUpdated);
ASSERT_EQ(957, context.m_coreStringData.m_cmpId);
ASSERT_EQ(0, context.m_coreStringData.m_cmpVersion);
ASSERT_EQ(0, context.m_coreStringData.m_consentScreen);
ASSERT_EQ("EN", context.m_coreStringData.m_consentLanguage);
ASSERT_EQ(15, context.m_coreStringData.m_vendorListVersion);
ASSERT_EQ(2, context.m_coreStringData.m_tcfPolicyVersion);
ASSERT_FALSE(context.m_coreStringData.m_isServiceSpecific);
ASSERT_FALSE(context.m_coreStringData.m_useNonStandardStacks);
ASSERT_FALSE(context.m_coreStringData.m_purposeOneTreatment);
ASSERT_FALSE(context.m_coreStringData.m_purposeOneTreatment);
ASSERT_EQ("AA", context.m_coreStringData.m_publisherCC);

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 6, {687, 688, 707, 708, 711, 712});
PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests, 6, {8, 12, 18, 23, 37, 42});
//this case has 79 true vendors which we only verify 5 of them
PrivacyTestUtil::verifyMapContent(*context.m_disclosedVendorListInfo, 79, {2, 6, 8, 719, 720});
}

TEST(VERIFY_PARSING_STRING_WITH_PUB_RESTRICTIONS, PARSE_STRING)
{
IabGdprUserConsentSingleton::instance().decodeOptionalParts = true;
TCFV2Context context;
std::string consentString = "CLSYjTaOngnCIAOABBENAXCMAGOAABBAAA7IA5n-m7fP6_3fbqVv6E__PoA5Aqff3aJx8tv_1967rfnQEQoAIAAQCrwkAEABAcACABIMACAAuApEVABABUSABgBCAVSAtIoACACIArYQAHACgAFgAVwBJgDcAI7AWgMAAgBiKgAgBMgFfKQAQBRkQAQA4AFiBAAoA-AEVAJlAVgAtYcACAJcAr4eABAK8OgBgFzAOmAqwgABAWyA.IFukWSQh";
TCFV2Decoder::parseConsentString(context, consentString);

ASSERT_EQ(2, context.m_coreStringData.m_version);
ASSERT_EQ(12119586010, context.m_coreStringData.m_createdAt);
ASSERT_EQ(15695245448, context.m_coreStringData.m_lastUpdated);
ASSERT_EQ(14, context.m_coreStringData.m_cmpId);
ASSERT_EQ(1, context.m_coreStringData.m_cmpVersion);
ASSERT_EQ(1, context.m_coreStringData.m_consentScreen);
ASSERT_EQ("EN", context.m_coreStringData.m_consentLanguage);
ASSERT_EQ(23, context.m_coreStringData.m_vendorListVersion);
ASSERT_EQ(2, context.m_coreStringData.m_tcfPolicyVersion);
ASSERT_FALSE(context.m_coreStringData.m_isServiceSpecific);
ASSERT_FALSE(context.m_coreStringData.m_useNonStandardStacks);
ASSERT_FALSE(context.m_coreStringData.m_purposeOneTreatment);
ASSERT_EQ("HZ", context.m_coreStringData.m_publisherCC);


PrivacyTestUtil::verifyBitsetContent(context.m_coreStringData.m_specialFeatureOptIns, 12, {0, 1});
PrivacyTestUtil::verifyBitsetContent(context.m_coreStringData.m_purposesConsent, 24, {1, 2, 6, 7, 8});
PrivacyTestUtil::verifyBitsetContent(context.m_coreStringData.m_purposesLITransparency, 24, {3, 9});

PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorConsents, 83, {2, 3, 4, 115});
PrivacyTestUtil::verifyMapContent(context.m_coreStringData.m_vendorInterests, 77, {5 , 7, 9, 12});

auto pubMap = context.m_coreStringData.purposeIdToPublisherRestriction;
for (auto elem : {2 , 3, 4, 5, 6, 7, 8, 9, 10 }) {
    std::cout<<"elem : "<< elem << std::endl;
    for (uint32_t restrictionType = 0; restrictionType < 4; restrictionType++) {
        auto key = std::make_pair(elem, restrictionType);
        auto ptr = pubMap->find(key);
        if (ptr != pubMap->end()) {
        auto pubEntry = ptr->second;
        std::cout<<"m_purposeId : "<<pubEntry->m_purposeId <<std::endl;
        std::cout<<"m_restrictionType : "<<pubEntry->m_restrictionType <<std::endl;
            for (auto vendorId : *pubEntry->m_restrictedVendors) {
                std::cout<<"vendorId : "<< vendorId<<std::endl;
            }
        } else {
        //        FAIL() << "element " << elem << " is not in map";
        }
    }
}

ASSERT_EQ(17, pubMap->size());

PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(2, 0), {248, 554, 613, 688, 726});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(2, 1), {8});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(2, 2), {2, 687});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(3, 0), {23, 657});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(3, 2), {18});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(4, 0), {80, 88, 174, 294, 440, 571, 720});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(5, 1), {42});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(6, 0), {98});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(7, 0), {302, 702});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(7, 1), {371, 467, 684});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(7, 2), {350});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(8, 0), {729});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(8, 2), {224, 708});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(9, 0), {722, 66, 681});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(10, 0), {694, 68});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(10, 1), {163});
PrivacyTestUtil::verifyPubMapContent(pubMap, std::make_pair(10, 2), {351, 153});
}
