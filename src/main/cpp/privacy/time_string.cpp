// @version: $Id: time_string.cpp 1 2009-12-09 19:05:10Z vli $

#include "time_string.h"
#include <cstring>
#include <boost/date_time/c_time.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include "mem_buffer.h"
//#include <cstdio>

using namespace MM::Utils;
using namespace boost::date_time;

/*
const char* TimeString::getLocalTimeZoneAbbreviation(tm* local, char* pOutput)
{
	char buf[64];
	if (strftime(buf, sizeof(buf), "%Z", local) <= 4) {
		strcpy(pOutput, buf);
	}
	else {
		// %Z must return the full name of the time zone
		pOutput[0] = buf[0];
		const char* p = buf + 1;
		int n = 0;
		while (n++ < 2) {
			p = strchr(p, ' ');
			if (NULL != p && *(++p)) {
				pOutput[n] = *p;
			}
			else
				break;
		}
		pOutput[n] = '\0';
	}
	return pOutput;
}
*/

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//		HTML
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
const char* TimeString::gmTimeForHTML(time_t ltime)
{
    m_buffer[0] = '\0';
    tm t;
    struct tm *gmt = c_time::gmtime(&ltime, &t);
    strftime(m_buffer, sizeof(m_buffer) - 1, "%a, %d %b %Y %H:%M:%S GMT", gmt);
    return m_buffer;
}

time_t TimeString::httpDateToUnixTime(const char* httpDate)
{
    if (httpDate) {
        static const char format[] = "%a, %d %b %Y %H:%M:%S %Z"; // rfc 1123
        struct tm tm;
        bzero(&tm, sizeof(tm));
        if (strptime(httpDate, format, &tm)) {
//            return mktime(&tm);
            return timegm(&tm);
        }
    }
    return 0;
}

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//		Message Log
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
const char* TimeString::gmTimeForMsgLog(time_t ltime)
{
    m_buffer[0] = '\0';
    if (ltime) {
        tm t;
        struct tm *gmt = c_time::gmtime(&ltime, &t);
        if (gmt) {
            strftime(m_buffer, sizeof(m_buffer) - 1, "%Y-%m-%d %H:%M:%S", gmt);
        }
    }
    return m_buffer;
}

const char* TimeString::localTimeForMsgLog(time_t ltime)
{
    m_buffer[0] = '\0';
    if (ltime) {
        tm t;
        struct tm *local = c_time::localtime(&ltime, &t);

        if (local) {
            strftime(m_buffer, sizeof(m_buffer) - 1,
                     "%m/%d/%Y %H:%M:%S", local);
        }
    }
    return m_buffer;
}

const char* TimeString::gmDateForMsgLog(time_t ltime)
{
    m_buffer[0] = '\0';
    if (ltime) {
        tm t;
        struct tm *gmt = c_time::gmtime(&ltime, &t);
        if (gmt) {
            strftime(m_buffer, sizeof(m_buffer) - 1, "%Y-%m-%d", gmt);
        }
    }
    return m_buffer;
}


// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//		Filename
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
const char* TimeString::gmTimeForFilename(time_t ltime)
{
    m_buffer[0] = '\0';
    if (ltime) {
        tm t;
        struct tm *gmt = c_time::gmtime(&ltime, &t);
        if (gmt) {
            strftime(m_buffer, sizeof(m_buffer) - 1, "%Y%m%d_%H%M%S", gmt);
        }
    }
    return m_buffer;
}

const char* TimeString::localTimeForFilename(time_t ltime)
{
    m_buffer[0] = '\0';
    if (ltime) {
        tm t;
        struct tm *local = c_time::localtime(&ltime, &t);

        if (local) {
            strftime(m_buffer, sizeof(m_buffer) - 1,
                     "%m%d%Y_%H%M%S", local);
        }
    }
    return m_buffer;
}


// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//		XML
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
const char* TimeString::gmDateTimeForXML(time_t ltime)
{
    m_buffer[0] = '\0';
    // CCYY-MM-DDThh:mm:ssZ
    tm t;
    struct tm *gmt = c_time::gmtime(&ltime, &t);
    strftime(m_buffer, sizeof(m_buffer)-1, "%Y-%m-%dT%H:%M:%SZ", gmt);
    return m_buffer;
}

const char* TimeString::gmTimeForXML(time_t ltime)
{
    m_buffer[0] = '\0';
    // CCYY-MM-DDThh:mm:ssZ
    tm t;
    struct tm *gmt = c_time::gmtime(&ltime, &t);
    strftime(m_buffer, sizeof(m_buffer)-1, "%H:%M:%SZ", gmt);
    return m_buffer;
}

const char* TimeString::humanTime(int secs)
{
    int t = secs;
    int s = t % 60;
    t /= 60;
    int m = t % 60;
    t /= 60;
    int h = t % 24;
    t /= 24;
    int d = t;

    MemBuffer out(m_buffer, sizeof(m_buffer));

    if (d > 0)
        out << d << "d ";
    if (h > 0 || out.length())
        out << h << "h ";
    if (m > 0 || out.length())
        out << m << "m ";
    out << s << "s";
    return m_buffer;
}

time_t TimeString::extractTimestampFromFile(const std::string& file)
{
    time_t stamp = 0;
    std::string realName;
    // extract time stamp from file name
    boost::filesystem::path real(realName);
    std::string filename = real.filename().native();

    size_t pos = filename.find_first_of('.');
    if (pos != std::string::npos) {
        std::string ts = filename.substr(pos + 1);
        struct tm time;
        char* result = strptime(ts.c_str(), "%Y-%m-%d_%H:%M:%S", &time);
        if (result != NULL) {
            stamp = timegm(&time);
        }
    }
    return stamp;
}

/*
const char* TimeString::localDateTimeForXML(time_t ltime)
{
	m_buffer[0] = '\0';
	// format:
	//	CCYY-MM-DDThh:mm:ss+/-hh:mm
	//	1991-06-12T00:00:00-04:00
	tm t;
	struct tm *local = c_time::localtime(&ltime, &t);
	int bias = getLocalToUTCBias(local->tm_isdst);
	size_t len = strftime(m_buffer, sizeof(m_buffer)-1,
						  "%Y-%m-%dT%H:%M:%S", local);
	len += sprintf(m_buffer+len, "%+03d:%02d", (bias / 60), (bias % 60));
	return m_buffer;
}


const char* TimeString::localTimeForXML(time_t ltime)
{
	m_buffer[0] = '\0';
	// format: hh:mm:ss+/-hh:mm
	tm t;
	struct tm *local = c_time::localtime(&ltime, &t);
	int bias = getLocalToUTCBias(local->tm_isdst);
	size_t len = strftime(m_buffer, sizeof(m_buffer)-1, "%H:%M:%S", local);
	len += sprintf(m_buffer+len, "%+03d:%02d", (bias / 60), (bias % 60));
	return m_buffer;
}

const char* TimeString::localDateForXML(time_t ltime)
{
	m_buffer[0] = '\0';
	// format: CCYY-MM-DD
	tm t;
	struct tm *local = c_time::localtime(&ltime, &t);
	strftime(m_buffer, sizeof(m_buffer)-1, "%Y-%m-%d", local);
	return m_buffer;
}
*/
