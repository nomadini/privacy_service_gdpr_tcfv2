#pragma once

// @version: $Id: smart_buffer.h 1 2009-12-09 19:05:10Z vli $

///
/// A simple class for efficiently allocating a small memory buffer.
/// If the requesting buffer size is less than 1k, the class uses a stack-based
/// buffer, avoids any memory allocation from heap.
/// If the requesting buffer is more than 1k, it then allocates from heap.
///
// @version: $Id: smart_buffer.h 1 2009-12-09 19:05:10Z vli $
//

namespace MM
{
    namespace Utils
    {


        class SmartBuffer
        {
        public:
            explicit SmartBuffer(int nReqSize);
            ~SmartBuffer();

            char*  getBuffer();
            const  char* getBuffer() const;

            operator char*();
            operator const char*() const;

        private:
            SmartBuffer() {}

        private:
            enum {
                e_buf_sz = 1024
            };

            char* m_pData;
            bool  m_bNew;
            char  m_buf[e_buf_sz];
        };

//
// ================================================================
//
        inline
        SmartBuffer::SmartBuffer(int nReqSize)
        {
            if (nReqSize <= e_buf_sz) {
                m_bNew = false;
                m_pData = m_buf;
            }
            else {
                m_bNew = true;
                m_pData = new char[nReqSize];
            }

        }

        inline
        SmartBuffer::~SmartBuffer()
        {
            if (m_bNew)
                delete[] m_pData;
        }

        inline
        char* SmartBuffer::getBuffer() {
            return m_pData;
        }

        inline
        const char* SmartBuffer::getBuffer() const {
            return m_pData;
        }

        inline
        SmartBuffer::operator char*() {
            return m_pData;
        }

        inline
        SmartBuffer::operator const char*() const {
            return m_pData;
        }


    }   // namespace Utils
}   // namespace ADP
