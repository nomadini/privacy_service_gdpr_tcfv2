//
// Created by Mahmoud Taabodi on 3/10/20.
//

#ifndef PRIVACY_SERVICE_GDPR_TCFV2_BASE64_UTIL_H
#define PRIVACY_SERVICE_GDPR_TCFV2_BASE64_UTIL_H

#include <boost/algorithm/string.hpp>
#include "mem_buffer.h"

namespace MM {
    namespace Utils {

        //TODO remove from core-bidder
        static std::string base64Decode(std::string &in, bool dothing) {
            boost::replace_all(in, "-", "+");
            boost::replace_all(in, "_", "/");

            std::string out;

            std::vector<int> T(256,-1);
            for (int i=0; i<64; i++) T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i;

            int val=0, valb=-8;
            for (char c : in) {
                if (T[c] == -1) break;
                val = (val<<6) + T[c];
                valb += 6;
                if (valb>=0) {
                    out.push_back(char((val>>valb)&0xFF));
                    valb-=8;
                }
            }
            return out;
        }
    }
}
#endif //PRIVACY_SERVICE_GDPR_TCFV2_BASE64_UTIL_H
