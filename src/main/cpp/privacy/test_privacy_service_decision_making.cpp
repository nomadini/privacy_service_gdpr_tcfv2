//
// Created by Mahmoud Taabodi on 3/4/20.
//

#include "gtest/gtest.h"
#include "privacy_service.h"
#include "iabGdprUserConsent.h"
#include "privacy_test_util.h"

// all consent strings can be decoded using this tool https://iabtcf.com/#/encode
TEST(HAVING_ALL_IMPORTANT_VENDOR_CONSENTS_IN_TRANSITION_PERIOD, MAKE_DECISION) {

TCFV2Context context;
std::string consentString = "COwg7Y_Owg7Y_DiAAAENAPCIAPIAAPIAAAAAABEAAiAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {no_restrictions});
}

TEST(HAVING_ALL_IMPORTANT_VENDOR_CONSENTS_BUT_GEO_LOCATION_IN_TRANSITION_PERIOD, MAKE_DECISION) {

TCFV2Context context;
std::string consentString = "COwg74mOwg74mOkAAAENAPCAAPIAAPIAAAAAABEAAiAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {no_precise_geo_location_usage});
}

TEST(HAVING_NO_CONSENT_BUT_ALL_LEGITIMATE_INTERESTS_TRANSITION_PERIOD, MAKE_DECISION) {

TCFV2Context context;
std::string consentString = "COwg9AbOwg9AbGoAAAENAPCMAAAAAP_AAAAAABEAAiAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {fully_restricted});
}

TEST(HAVING_ALL_VENDOR_CONSENTS_AND_GEO_LOCATION_BUT_NO_LEGITIMATE_INTERESTS_TRANSITION_PERIOD, MAKE_DECISION) {

TCFV2Context context;
std::string consentString = "COwg9VXOwg9VXAEAAAENAPCIAP_AAAAAAAAAABEAAAAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {fully_restricted});
}
//
//
// POST TRANSITION TESTS
//
//
TEST(HAVING_ALL_VENDOR_CONSENTS_IN_POST_TRANSITION_PERIOD, MAKE_DECISION) {

TCFV2Context context;
std::string consentString = "COwhDePOwhDePCwAAAENAPCIALIAAEIAAAAAABEAAiAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInPostTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {no_restrictions});
}

TEST(HAVING_ALL_VENDOR_CONSENTS_BUT_NO_SPECIAL_FEATURE_IN_POST_TRANSITION_PERIOD, MAKE_DECISION) {

TCFV2Context context;
std::string consentString = "COwhD2_OwhD2_OWAAAENAPCAALAAAEIAAAAAABEAAiAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInPostTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {no_precise_geo_location_usage});
}

TEST(HAVING_NO_CONSENT_BUT_ALL_LEGITIMATE_INTERESTS_POST_TRANSITION_PERIOD, MAKE_DECISION) {
TCFV2Context context;
std::string consentString = "COwg9AbOwg9AbGoAAAENAPCMAAAAAP_AAAAAABEAAiAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInPostTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {fully_restricted});
}

TEST(HAVING_ALL_VENDOR_CONSENTS_AND_GEO_LOCATION_BUT_NO_LEGITIMATE_INTERESTS_POST_TRANSITION_PERIOD, MAKE_DECISION) {

TCFV2Context context;
std::string consentString = "COwg9VXOwg9VXAEAAAENAPCIAP_AAAAAAAAAABEAAAAA.IFoEUQQgAIQwgIwQABAEAAAAOIAACAIAAAAQAIAgEAACEAAAAAgAQBAAAAAAAGBAAgAAAAAAAFAAECAAAgAAQARAEQAAAAAJAAIAAgAAAYQEAAAQmAgBC3ZAYzUw";
PrivacyService privacyService;
TCFV2Decoder::parseConsentString(context, consentString);
uint8_t vendorId = 2;
privacyService.getBiddingPrivacyRestrictionsInPostTransitionPeriod(vendorId, context);
PrivacyTestUtil::restrictionsEqualTo(context.privacyRestrictions, {fully_restricted});
}
