
#ifndef PRIVACY_SERVICE_GDPR_TCFV2_MEMBUFFER_H
#define PRIVACY_SERVICE_GDPR_TCFV2_MEMBUFFER_H

//
// A simple class that can be used as a string/ostringstream replacement, or an expandable text
// (and/or binary byte) buffer.
//
// One use case:
// Say in your function you need to constructor a message that goes to your message log,
// but you concern about memory allocation. In this case, you can do thing like this:
//
//  char buffer[1024];
//  // create a MemBuffer that is initalliy backed by the stack allocated buffer.
//  MemBuffer msg(buffer, sizeof(buffer));
//  msg << "Error in socket, ec=" << ::errno << ", msg=" << getSocketErrorMsg();
//
//
// @version: $Id: mem_buffer.h 34 2010-01-14 05:00:34Z vli $
//

//#include "../def/pstdint.h"
#include <cstdint>
#include <string>
#include <cstring>

namespace MM
{
    namespace Utils
    {
        class StatCounter {
        public:
            StatCounter(std::string name, long val) {
                //mock
            }
            void inc() {
                //mock
            }
        };
        class MemBuffer
        {
        public:
            enum {
                MAX_LEN_INT16  = 6,
                MAX_LEN_INT32  = 11,
                MAX_LEN_INT64  = 21,
                MAX_LEN_INT128 = 41
            };

            // helper functions
            //
            // convert integer to string.
            // return the length of the integer (including sign) -- i.e., the length of the string.
            //
            // note: length of buf must be at least 11 bytes
            static int to_str(int32_t i, char* buf);
            static int to_str(uint32_t u, char* buf);
            //
            // note: length of buf must be at least 21 bytes!
            static int to_str(int64_t i, char* buf);
            static int to_str(uint64_t u, char* buf);
            //
            // note: length of buf must be at least 41 bytes!
            static int to_str(__int128 i, char* buf_41);
            static int to_str(unsigned __int128 u, char* buf_41);
            //
            // reverse string [begin, end]
            static void reverseString(char* begin, char* end);

            typedef char            value_type;
            typedef char*           iterator;
            typedef const char*     const_iterator;

            // default: initial-size: 10k, grow-size: 5k
            // however, no memory is allocated yet until the object receives data!
            // (so very tiny overhead associate to the object)
            MemBuffer();
            MemBuffer(const MemBuffer& mb);
            /// move constructor
            MemBuffer(MemBuffer&& mb);

            // if nInitSize == 0, use default-initial-size (10k).
            // if nGrowSize == 0, use default-grow-size (5k).
            MemBuffer(unsigned int nInitSize, unsigned int nGrowSize = 0);
            // use the external buffer "pExtBuffer" as its initial buffer.
            // this is like the std::strstream, but is different to the latter
            // when the buffer is full -- in this case, it will allocate a new and
            // bigger buffer.
            // caller is responsible for the allocation/deallocation of the "pExtBuffer"
            MemBuffer(char* pExtBuffer, unsigned int nExtBufSize, unsigned int nGrowSize = 0);

            virtual ~MemBuffer();

            /// exchanges all internal data between me and "other"
            void swap(MemBuffer& other);

            /// reserve "size" amount of buffer space as capacity.
            void reserve(size_t size);

            /// release the internal buffer and give the ownership of the buffer to caller.
            /**
             * @param data [out]   - receives the location where the buffer begins
             * @param length [out] - receives the length of the buffer
             *
             * @return true if the buffer is successfully released.
             *         false otherwise (the buffer cannot be released if it is from external.
             */
            bool releaseBuffer(char*& data, int& length);

            // precision for floating number output
            void setDefaultPrecision(int pc);
            int  getDefaultPrecision() const;

            MemBuffer& operator=(const MemBuffer& mb);
            MemBuffer& operator=(MemBuffer&& mb);

            // replace the current content with the incoming string
            MemBuffer& operator=(const char* pStr);
            MemBuffer& operator=(const std::string& szStr);

            // special for string data only, make the object work like a string
            MemBuffer& operator+=(const std::string& szStr);
            MemBuffer& operator+=(const char* pStr);
            MemBuffer& operator+=(const unsigned char* pStr);

            MemBuffer& append(const void* pData, size_t nDataLen);

            // other helper methods
            MemBuffer& append(const std::string& str);
            MemBuffer& append(const char* pString);
            MemBuffer& append(const unsigned char* pString);
            // convert an integer to an ascii string and append to the stream
            MemBuffer& append(char byte);
            MemBuffer& append(unsigned char byte);
            MemBuffer& append(int16_t  number);
            MemBuffer& append(uint16_t number);
            MemBuffer& append(int32_t  number);
            MemBuffer& append(uint32_t number);
            MemBuffer& append(int64_t  number);
            MemBuffer& append(uint64_t number);
            MemBuffer& append(__int128  number);
            MemBuffer& append(unsigned __int128 number);

#if defined(WIN32) || defined(WIN64) || !defined(__LP64__)
            MemBuffer& append(long number);
            MemBuffer& append(unsigned long number);
#endif
//    MemBuffer& append(int number);
//    MemBuffer& append(unsigned int number);
#if defined(__x86_64__)
            MemBuffer& append(long long number);
#endif

            MemBuffer& appendHex(uint8_t  number);
            MemBuffer& appendHex(uint16_t number);
            MemBuffer& appendHex(uint32_t number);
            MemBuffer& appendHex(uint64_t number);
            MemBuffer& appendHex(const std::string& str);

            // convert a float number to an ascii string and append to the stream
            MemBuffer& append(float number, int precision = 0);
            MemBuffer& append(double number, int precision = 0);

            MemBuffer& append(bool value);

            // appends number in its raw (binary) form to the buffer.
            MemBuffer& appendRaw(char byte);
            MemBuffer& appendRaw(unsigned char byte);
            MemBuffer& appendRaw(int16_t  number);
            MemBuffer& appendRaw(uint16_t number);
            MemBuffer& appendRaw(int32_t  number);
            MemBuffer& appendRaw(uint32_t number);
            MemBuffer& appendRaw(int64_t  number);
            MemBuffer& appendRaw(uint64_t number);
#if defined(WIN32) || defined(WIN64) || !defined(__LP64__)
            MemBuffer& appendRaw(long number);
            MemBuffer& appendRaw(unsigned long number);
#endif
            MemBuffer& appendRaw(float number);
            MemBuffer& appendRaw(double number);
#if defined(__x86_64__)
            MemBuffer& appendRaw(long long number);
#endif

            MemBuffer& operator<<(const std::string& string);
            MemBuffer& operator<<(const char* pString);
            MemBuffer& operator<<(const unsigned char* pString);
            MemBuffer& operator<<(char byte);
            MemBuffer& operator<<(unsigned char byte);
            MemBuffer& operator<<(int16_t  number);
            MemBuffer& operator<<(uint16_t number);
            MemBuffer& operator<<(int32_t  number);
            MemBuffer& operator<<(uint32_t number);
            MemBuffer& operator<<(int64_t  number);
            MemBuffer& operator<<(uint64_t number);

#if defined(WIN32) || defined(WIN64) || !defined(__LP64__)
            MemBuffer& operator<<(long     number);
            MemBuffer& operator<<(unsigned long number);
#endif
            MemBuffer& operator<<(__int128  number);
            MemBuffer& operator<<(unsigned __int128 number);
            MemBuffer& operator<<(float number);
            MemBuffer& operator<<(double number);
#if defined(__x86_64__)
            MemBuffer& operator<<(long long number);
#endif
            MemBuffer& operator<<(bool value);

            // the returned data (stream) is null-terminated, even when the data are binary.
            // (for binary data, you need to call length() to get the size of the data).
            const void* data() const;
            // just the return type is different
            const char* string() const;
            // return the length of the data
            unsigned int length() const;
            // return the capacity of the buffer.
            unsigned int capacity() const;

            /// return true if the buffer is empty
            bool empty() const;

            // total memory footprint that this object is taking
            size_t  footprint() const;

            operator const char*() const;

            // truncate the buffer to the new length.
            // the function has no effect to the data if nNewLen > used length
            void  truncate(unsigned int nNewLen);
            // reset this object -- the buffered data will be lost
            void  clear();
            // will take effect in next re-allocation
            void  setGrowSize(const unsigned int nGrowSize);

            // to implement functions to make it compatiable to back_inserter interface
            //
            /// returns the index (iterator) pointing to the past-the-end character of the data.
            /// return value could be null, when there is no data yet
            iterator end();
            const_iterator end() const;
            //
            /// insert a copy of "n" characters pointed by "s"
            MemBuffer& insert(iterator pos, const char* s, size_t n);
            /// inserts a copy of characters in the range [first,last), in the same order.
            MemBuffer& insert(iterator pos, const char* first, const char* last);
            //
            void push_back(char byte);

            //
            // methods that are designed for using google snappy compressor. do not use
            // them unless you are absolutely know what you are doing!
            ///
            /*
             * return the internal byte-buffer that is used by this class.
             * caller can modify the content of the buffer. Caller is also responsible
             * for ensuring it will not over-run the buffer (i.e., need to check capacity)
             */
            char* getInternalBuffer();
            //
            ///
            /*
             * set how many bytes the buffer has been used.
             * if the new size is smaller than the existing size, the content of the buffer will be truncated.
             * if the new size is larger than the existing size, it will adjust the buffer to accommodate the
             * asked size, this may result grow the buffer.
             * after the function return, length() == size.
             */
            void resize(size_t size);

        private:
            // default values
            enum {
                e_def_init_size = 10240,
                e_def_grow_size = 5120
            };

            void growIfNeeded(size_t nReqSize);

        private:
            char  m_internalBuf[256];   // to prevent small buffers from allocating
            char* m_pData;
            char* m_pCurAt; // points to the end of the buffered data
            unsigned int m_nBufSize;
            // this is always true: m_nUsedSize <= m_nBufSize-1
            unsigned int m_nUsedSize;
            unsigned int m_nGrowSize;
            bool m_bOwnTheBuffer;   // if the m_pData is owned by "this"
            // it is false when the m_pData is an external buffer.
            int   m_defPrecision;   // precision for floating number output
        };

//
// inline functions
        inline MemBuffer& MemBuffer::operator+=(const std::string& str)
        {
            return append(str);
        }
        inline MemBuffer& MemBuffer::operator+=(const char* pString)
        {
            return append(pString);
        }
        inline MemBuffer& MemBuffer::operator+=(const unsigned char* pString)
        {
            return append(pString);
        }

        inline MemBuffer& MemBuffer::append(const std::string& str)
        {
            return append(str.data(), str.size());
        }
        inline MemBuffer& MemBuffer::append(const char* pString)
        {
            return pString ? append(pString, strlen(pString)) : *this;
        }
        inline MemBuffer& MemBuffer::append(const unsigned char* pString)
        {
            return pString ? append(pString, strlen((const char*)pString)) : *this;
        }
        inline MemBuffer&  MemBuffer::append(char byte)
        {
            return append(&byte, sizeof(byte));
        }
        inline MemBuffer& MemBuffer::append(unsigned char byte)
        {
            return append(&byte, sizeof(byte));
        }
/*
inline MemBuffer& MemBuffer::append(int number)
{
    return append(static_cast<int32_t>(number));
}

inline MemBuffer& MemBuffer::append(unsigned int number)
{
    return append(static_cast<uint32_t>(number));
}
*/

#if defined(WIN32) || defined(WIN64) || !defined(__LP64__)

        inline MemBuffer& MemBuffer::append(long number)
        {
            return append(static_cast<int32_t>(number));
        }

        inline MemBuffer& MemBuffer::append(unsigned long number)
        {
            return append(static_cast<uint32_t>(number));
        }
#endif

#if defined(__x86_64__)
        inline MemBuffer& MemBuffer::append(long long number)
{
    return append(static_cast<int64_t>(number));
}
#endif

        inline MemBuffer& MemBuffer::append(bool value) {
            return append(value ? "true" : "false");
        }

        inline MemBuffer& MemBuffer::operator<<(const std::string& str)
        {
            return append(str);
        }
        inline MemBuffer& MemBuffer::operator<<(const char* pString)
        {
            return append(pString);
        }
        inline MemBuffer& MemBuffer::operator<<(const unsigned char* pString)
        {
            return append(pString);
        }
        inline MemBuffer& MemBuffer::operator<<(int32_t number)
        {
            return append(number);
        }
        inline MemBuffer& MemBuffer::operator<<(uint32_t number)
        {
            return append(number);
        }

#if defined(WIN32) || defined(WIN64) || !defined(__LP64__)
        inline MemBuffer& MemBuffer::operator<<(long number)
        {
            return append(number);
        }
        inline MemBuffer& MemBuffer::operator<<(unsigned long number)
        {
            return append(number);
        }
#endif

#if defined(__x86_64)
        inline MemBuffer& MemBuffer::operator<<(long long number)
{
    return append(number);
}
#endif

        inline MemBuffer& MemBuffer::operator<<(char byte)
        {
            return append(byte);
        }
        inline MemBuffer& MemBuffer::operator<<(unsigned char byte)
        {
            return append(byte);
        }

        inline MemBuffer& MemBuffer::operator<<(int64_t number)
        {
            return append(number);
        }
        inline MemBuffer& MemBuffer::operator<<(uint64_t number)
        {
            return append(number);
        }
        inline MemBuffer& MemBuffer::operator<<(__int128  number) {
            return append(number);
        }
        inline MemBuffer& MemBuffer::operator<<(unsigned __int128 number) {
            return append(number);
        }

        inline MemBuffer& MemBuffer::operator<<(int16_t number)
        {
            return append(number);
        }
        inline MemBuffer& MemBuffer::operator<<(uint16_t number)
        {
            return append(number);
        }

        inline MemBuffer& MemBuffer::operator<<(float number)
        {
            return append(number, m_defPrecision);
        }

        inline MemBuffer& MemBuffer::operator<<(double number)
        {
            return append(number, m_defPrecision);
        }

        inline MemBuffer& MemBuffer::operator<<(bool value) {
            return append(value);
        }


        inline MemBuffer& MemBuffer::appendRaw(char byte) {
            return append(byte);
        }
        inline MemBuffer& MemBuffer::appendRaw(unsigned char byte) {
            return append(byte);
        }
        inline MemBuffer& MemBuffer::appendRaw(int16_t number) {
            return append(&number, sizeof(int16_t));
        }
        inline MemBuffer& MemBuffer::appendRaw(uint16_t number) {
            return append(&number, sizeof(uint16_t));
        }
        inline MemBuffer& MemBuffer::appendRaw(int32_t  number) {
            return append(&number, sizeof(int32_t));
        }
        inline MemBuffer& MemBuffer::appendRaw(uint32_t number) {
            return append(&number, sizeof(uint32_t));
        }
        inline MemBuffer& MemBuffer::appendRaw(int64_t  number) {
            return append(&number, sizeof(int64_t));
        }
        inline MemBuffer& MemBuffer::appendRaw(uint64_t number) {
            return append(&number, sizeof(uint64_t));
        }

#if defined(WIN32) || defined(WIN64) || !defined(__LP64__)
        inline MemBuffer& MemBuffer::appendRaw(long number) {
            return append(&number, sizeof(long));
        }
        inline MemBuffer& MemBuffer::appendRaw(unsigned long number) {
            return append(&number, sizeof(unsigned long));
        }
#endif

#if defined(__x84_64__)
        inline MemBuffer& MemBuffer::appendRaw(long long number) {
    return append(&number, sizeof(long long));
}
#endif

        inline MemBuffer& MemBuffer::appendRaw(float number) {
            return append(&number, sizeof(float));
        }
        inline MemBuffer& MemBuffer::appendRaw(double number) {
            return append(&number, sizeof(double));
        }

        inline void MemBuffer::push_back(char byte) {
            append(byte);
        }

        inline void MemBuffer::truncate(unsigned int nNewLen) {
            if (nNewLen < m_nUsedSize) {
                m_nUsedSize = nNewLen;
                m_pCurAt = m_pData + m_nUsedSize;
                *m_pCurAt = '\0';
            }
        }

        inline void MemBuffer::clear() {
            m_nUsedSize = 0;
            m_pCurAt = m_pData;
            if (m_pCurAt) {
                *m_pCurAt = '\0';
            }
        }

        inline void MemBuffer::setGrowSize(unsigned int nGrowSize) {
            m_nGrowSize = nGrowSize;
        }

        inline const void* MemBuffer::data() const {
            return m_pData;
        }
        inline unsigned int MemBuffer::length() const {
            return m_nUsedSize;
        }

        inline const char* MemBuffer::string() const
        {
            return m_pData ? m_pData : "";
        }

        inline
        char* MemBuffer::getInternalBuffer() {
            return m_pData;
        }

        inline unsigned int MemBuffer::capacity() const {
            return m_nBufSize;
        }

        inline size_t MemBuffer::footprint() const {
            return sizeof(*this) + m_nBufSize * sizeof(char);
        }

        inline MemBuffer::operator const char*() const {
            return string();
        }

        inline void MemBuffer::setDefaultPrecision(int pc) {
            m_defPrecision = pc;
        }
        inline int MemBuffer::getDefaultPrecision() const {
            return m_defPrecision;
        }

        inline
        MemBuffer::iterator MemBuffer::end() {
            return m_pData + m_nUsedSize;
        }
        inline
        MemBuffer::const_iterator MemBuffer::end() const {
            return m_pData + m_nUsedSize;
        }

        inline
        bool MemBuffer::empty() const {
            return 0 == m_nUsedSize;
        }


    }   // namespace Utils
}   // namespace MM

#endif