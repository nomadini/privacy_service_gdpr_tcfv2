//
// Created by Mahmoud Taabodi on 3/6/20.
//

#ifndef ADVANCED_CACHE_IABGDPRUSERCONSENT_H
#define ADVANCED_CACHE_IABGDPRUSERCONSENT_H

#include <string>
#include <memory>
#include <iostream>
#include <boost/dynamic_bitset.hpp>
#include "iabGdprUserConsentV1.h"

class IabGdprUserConsent {

    public:
    //https://stackoverflow.com/questions/708114/convert-byte-array-into-bitset
    static void convertBytesToBitset(const std::string& incomingDaisyStr, boost::dynamic_bitset<>& bitSet) {
        size_t numBytes = incomingDaisyStr.size();
        for(size_t i = 0; i < numBytes; ++i)
        {
            //our liutils decoder only works with signed chars.
            //however, we are doing bit logic here with a signed char
            //so we use reinterpret cast to get the right bits
            char tmp  = incomingDaisyStr[i];
            unsigned char cur = reinterpret_cast<unsigned char&>(tmp);
            int offset = i * 8 + 7;

            for(int bit = 0; bit < 8; ++bit)
            {
                bitSet[offset] = cur & 1;
                --offset;  // Move to next bit in bitSet
                cur >>= 1; // Move to next bit in array
            }
        }
    }

};



#endif //ADVANCED_CACHE_IABGDPRUSERCONSENT_H
