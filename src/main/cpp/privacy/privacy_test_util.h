//
// Created by Mahmoud Taabodi on 3/6/20.
//

#ifndef PRIVACY_TEST_UTIL_H
#define PRIVACY_TEST_UTIL_H

#include "gtest/gtest.h"
#include "privacy_service.h"
#include <unordered_map>
#include <unordered_set>
#include <initializer_list>
#include <boost/dynamic_bitset.hpp>
using namespace MM::Privacy;

class PrivacyTestUtil
{
public:
    static std::string bitSetToString(const boost::dynamic_bitset<>& bitset)
    {
        std::string str;
        for (std::size_t i = 0; i < bitset.size(); i++)
        {
            if (bitset[i] == 1)
            {
                str.push_back('1');
            }
            else
            {
                str.push_back('0');
            }
        }
        return str;
    }

    static void printBitMap(const std::unordered_map<uint32_t, bool>& map, std::string name)
    {
        std::cout << name << " : " << bitMapToString(map) << std::endl;
    }

    static void printBitSet(boost::dynamic_bitset<> bitset, std::string name)
    {
        std::cout << name << " extracted is " << std::endl;
        for (std::size_t i = 0; i < bitset.size(); i++)
        {
            std::cout << bitset[i];
        }
        std::cout << std::endl;
    }

    static std::string bitMapToString(const std::unordered_map<uint32_t, bool>& map)
    {
        std::string str;
        for (auto const &entry : map)
        {
            str += "e" + std::to_string(entry.first) + ":";
            if (entry.second == 1)
            {
                str.push_back('1');
            }
            else
            {
                str.push_back('0');
            }
            str.push_back('_');
        }
        return str;
    }

    static void restrictionsEqualTo(const RestrictionSetType& restrictionSet,
                                    const std::initializer_list<PrivacyRestriction> &candidates) {
        ASSERT_FALSE(restrictionSet.empty());
        ASSERT_FALSE(candidates.size() == 0);
        if (candidates.size() != restrictionSet.size()) {
            FAIL() << "restrictionSet is "<< restrictionSet.size() << " vs expected size : "<< candidates.size();
        };

        for (auto& elem : candidates) {
            ASSERT_TRUE(restrictionSet.find(elem) != restrictionSet.end());
        }
    }

    static void verifyBitsetContent(const boost::dynamic_bitset<>& bitset,
                                    const int expectedSize,
                                    const std::unordered_set<int> &trueIndices)
    {
        ASSERT_EQ(expectedSize, bitset.size());
        for (int i = 0; i < expectedSize; i++)
        {
            if (trueIndices.find(i) == trueIndices.end()) {
                // bit at index i th shall not be true
                ASSERT_FALSE(bitset[i]);
            }
        }
        for (auto elem : trueIndices)
        {
            if ((int)bitset.size() > elem)
            {
                ASSERT_TRUE(bitset[elem]);
            }
            else
            {
                FAIL() << "bit " << elem << " is not in bitset";
            }
        }
    }


    static void verifyPubMapContent(std::unordered_map<std::pair<uint32_t, uint32_t>, PubRestrictionEntry*, boost::hash<std::pair<uint32_t, uint32_t>>>* pubMap,
    std::pair<uint32_t, uint32_t> key,
    const std::initializer_list<int> &restrictedVendors) {
        auto pairPtr = pubMap->find(key);

        ASSERT_TRUE(pairPtr != pubMap->end());
        ASSERT_EQ(key.first, pairPtr->second->m_purposeId);
        ASSERT_EQ(key.second, pairPtr->second->m_restrictionType);
        ASSERT_FALSE(pairPtr->second->m_restrictedVendors->empty());

        for (auto elem : restrictedVendors) {
            ASSERT_TRUE(pairPtr->second->m_restrictedVendors->find(elem) != pairPtr->second->m_restrictedVendors->end());
        }
    }

    static void verifyMapContent(const std::unordered_map<uint32_t, bool>& consentMap,
                                 const int expectedMapSize,
                                 const std::initializer_list<int> &trueIndices)
    {
        ASSERT_EQ(expectedMapSize, consentMap.size());
        for (auto elem : trueIndices)
        {
            auto ptr = consentMap.find(elem);
            if (ptr != consentMap.end())
            {
                ASSERT_TRUE(ptr->second);
            }
            else
            {
                FAIL() << "element " << elem << " is not in map";
            }
        }
    }
};
#endif //PRIVACY_TEST_UTIL_H
