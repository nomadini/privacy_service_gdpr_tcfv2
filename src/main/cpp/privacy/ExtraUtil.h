//
// Created by Mahmoud Taabodi on 3/10/20.
//

#ifndef PRIVACY_SERVICE_GDPR_TCFV2_EXTRAUTIL_H
#define PRIVACY_SERVICE_GDPR_TCFV2_EXTRAUTIL_H

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <boost/dynamic_bitset.hpp>
#include <unordered_map>


using namespace std;

class ExtraUtil {
public:
    static void printContext(const MM::Privacy::TCFV2Context& context) {
        MM::Utils::MemBuffer output;
        context.report(output);
        std::string outStr = output.string();
        std::cout<<"output.string size "<< outStr.size() <<std::endl;
        std::cout<<"output "<< outStr <<std::endl;
    }

    static std::string bitSetToString(boost::dynamic_bitset<> bitset) {
        std::string str;
        for (std::size_t i = 0; i < bitset.size(); i++) {
            if (bitset[i] == 1) {
                str.push_back('1');
            } else {
                str.push_back('0');
            }
        }
        return str;
    }

    static void printBitMap(std::unordered_map<uint32_t, bool> map, std::string name) {
        std::cout<<name << " : " << bitMapToString(map)<< std::endl;
    }

    static void printBitSet(boost::dynamic_bitset<> bitset, std::string name) {
        std::cout<<name << " extracted is "<<std::endl;
        for (std::size_t i = 0; i < bitset.size(); i++) {
            std::cout<<bitset[i];
        }
        std::cout<<endl;
    }

    static std::string bitMapToString(std::unordered_map<uint32_t, bool> map) {
        std::string str;
        for (auto const& entry: map) {
            str +=  "e" + std::to_string(entry.first) + ":";
            if (entry.second == 1) {
                str.push_back('1');
            } else {
                str.push_back('0');
            }
            str.push_back('_');
        }
        return str;
    }
};
#endif //PRIVACY_SERVICE_GDPR_TCFV2_EXTRAUTIL_H
