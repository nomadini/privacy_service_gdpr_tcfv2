// @version: $Id: time_utils.cpp 21 2009-12-23 17:43:04Z vli $

#include "time_utils.h"
#include <boost/date_time/c_time.hpp>

#if defined(WIN32) || defined(WIN64)
#include <windows.h>
#else
#include <sys/times.h>
#include <sys/time.h>
#endif

#ifndef CLK_TCK
// ugly! but CLK_TCK does not share the same value with CLOCKS_PER_SEC!!!
#define CLK_TCK 100
#endif


namespace MM {
namespace Utils {


// should not use this function at all!
unsigned long getMilliSecond() {

#if defined(WIN32) || defined(WIN64)
    return GetTickCount();
#else
    timeval now;
    gettimeofday(&now, NULL);
    return ((unsigned long)now.tv_sec * 1000UL) + ((unsigned long)now.tv_usec / 1000UL);
#endif
}


// a c-runtime "time(0)" function replacement with millisecond accuracy.
// time(0) is not cheap, and if you codes need to call it thousands of times/sec,
// consider using this replacement.
time_t fast_time_t(int* ms)
{
    unsigned long now  = getMilliSecond();
	if (ms != NULL) {
		*ms = (int)(now % 1000UL); // result fits in int
    }
    return (time_t)(now / 1000UL);
}

using namespace boost::date_time;

time_t to_hour_start(time_t now)
{
    struct tm t;
    c_time::localtime(&now, &t);
    t.tm_sec = t.tm_min = 0;
    return mktime(&t);
}

time_t to_day_start(time_t now)
{
    struct tm t;
    c_time::localtime(&now, &t);
    t.tm_sec = t.tm_min = t.tm_hour = 0;
    return mktime(&t);
}

//localtime & mktime is a pair,
//gmtime & timegm is a pair!!!
time_t to_gmt_day_start(time_t now)
{
    struct tm t;
    c_time::gmtime(&now, &t);
    t.tm_sec = t.tm_min = t.tm_hour = 0;
    return timegm(&t);
}


time_t to_week_start(time_t now)
{
    struct tm t;
    c_time::localtime(&now, &t);
    t.tm_mday -= t.tm_wday;
    t.tm_yday -= t.tm_wday;
    t.tm_sec = t.tm_min = t.tm_hour = t.tm_wday = 0;
    return mktime(&t);
}

time_t to_month_start(time_t now)
{
    struct tm t;
    c_time::localtime(&now, &t);
    t.tm_yday -= (t.tm_mday + 1); 
    t.tm_mday = 1;
    t.tm_sec = t.tm_min = t.tm_hour = 0;
    return mktime(&t);
}

int day_in_year(time_t now)
{
    struct tm t;
    c_time::localtime(&now, &t);
    return t.tm_yday;
}

// return true if tm1 and tm2 in the same gmt day
bool same_day(time_t tm1, time_t tm2)
{
    return (to_day_start(tm1) - to_day_start(tm2) == 0);
}

}
}
