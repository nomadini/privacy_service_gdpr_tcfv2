
#include "privacy_service.h"
#include "bidderConfig.h"
#include "iabGdprUserConsent.h"
#include "time_string.h"
#include <bitset>
#include <boost/algorithm/string.hpp>

using namespace MM::Privacy;

uint32_t CoreStringData::BEGINNING_OF_CORE = 0;
uint32_t CoreStringData::VERSION_LENGTH = 6;
uint32_t CoreStringData::CREATE_LENGTH = 36;
uint32_t CoreStringData::UPDATED_LENGTH = 36;
uint32_t CoreStringData::CMP_ID_LENGTH = 12;
uint32_t CoreStringData::CMP_VERSION_LENGTH = 12;
uint32_t CoreStringData::CONSENT_SCREEN_LENGTH = 6;
uint32_t CoreStringData::CONSENT_LANGUAGE_LENGTH = 6;
uint32_t CoreStringData::VENDOR_LIST_VERSION_LENGTH = 12;
uint32_t CoreStringData::TCF_POLICY_VERSION_LENGTH = 6;
uint32_t CoreStringData::IS_SERVER_SPECIFIC_LENGTH = 1;
uint32_t CoreStringData::USE_NON_STANDARD_LENGTH = 1;
uint32_t CoreStringData::SPECIAL_FEATURE_OPT_INS_LENGTH = 12;
uint32_t CoreStringData::PURPOSE_CONTENT_LENGTH = 24;
uint32_t CoreStringData::PURPOSE_LI_TRANSPARENCY_LENGTH = 24;
uint32_t CoreStringData::PURPOSE_ON_TREATMENT_LENGTH = 1;
uint32_t CoreStringData::PUBLISHER_CC_LANGUAGE_LENGTH = 6;
uint32_t TCFV2Decoder::MAX_VENDOR_ID_LENGTH = 16;
uint32_t TCFV2Decoder::IS_RANGE_ENCODING_LENGTH = 1;
uint32_t TCFV2Decoder::NUMBER_OF_ENTRIES_LENGTH = 12;
uint32_t TCFV2Decoder::IS_RANGE_LENGTH = 1;
uint32_t TCFV2Decoder::START_OR_ONLY_VENDOR_ID_LENGTH = 16;
uint32_t TCFV2Decoder::NUMB_PUB_RESTRICTIONS_LENGTH = 12;
uint32_t TCFV2Decoder::SEGMENT_TYPE_LENGTH = 3;
uint32_t TCFV2Decoder::PURPOSE_ID_LENGTH = 6;
uint32_t TCFV2Decoder::PUB_RESTRICTION_TYPE_LENGTH = 2;

PrivacyService *PrivacyService::m_inst = nullptr;
std::mutex PrivacyService::s_mutex;
std::string TCFV2Context::m_mapDivStyle =
        "<div style=\"display: grid; grid-template-columns: 20px 50px; grid-gap: 12px;"
        "max-width:150px;max-height: 300px;overflow: auto;\">";
MM::Privacy::CoreStringData::CoreStringData() {}
MM::Privacy::TCFV2Decoder::TCFV2Decoder() {}
MM::Privacy::PubRestrictionEntry::PubRestrictionEntry() {}
MM::Privacy::TCFV2Context::TCFV2Context() {}

std::string MM::Privacy::Util::getPubRestrictionTypeName(const PubRestrictionType& type) {
    switch (type) {
        case not_allowed:
            return "not_allowed";
        case requires_consent:
            return "requires_consent";
        case requires_legitimate_interest:
            return "requires_legitimate_interest";
        case undefined:
            return "undefined";
        default:
            return "uknownType";
    }
}

std::string MM::Privacy::Util::getErrorTypeName(const ErrorType& type) {
    switch (type) {
        case noError:
            return "noError";
        case badSegmentType:
            return "badSegmentType";
        case errorInDecodingCoreString:
            return "errorInDecodingCoreString";
        case errorInDecodingDisclosedVendors:
            return "errorInDecodingDisclosedVendors";
        case errorInDecodingAllowedVendorsList:
            return "errorInDecodingAllowedVendorsList";
        default:
            return "unknownError";
    }
}

std::vector<std::string> MM::Privacy::Util::getPrivacyRestrictionInfo(const RestrictionSetType& restrictions) {

    std::vector<std::string> descrs;
    for (auto rest : restrictions) {
        switch (rest) {
            case no_restrictions:
                descrs.push_back("no_restrictions");
                break;
            case no_precise_geo_location_usage:
                descrs.push_back("no_precise_geo_location_usage");
                break;
            case fully_restricted:
                descrs.push_back("fully_restricted");
                break;
            default:
                descrs.push_back("unknown_restriction");
                break;
        }
    }
    return descrs;
}

void MM::Privacy::TCFV2Decoder::parseConsentString(TCFV2Context& context, const std::string& consentStringArg, const bool& decodeOptionalParts) {
    context.m_consentStringBase64 = consentStringArg;

    std::vector<std::string> consentsParts;
    boost::split(consentsParts, consentStringArg, boost::is_any_of("."));

    uint32_t readAtPos = 0;
    //the first part of consent always has to be core
    std::string decodedCoreString = MM::Utils::base64Decode(consentsParts.at(0), true);
    boost::dynamic_bitset<> allBitSet(8 * decodedCoreString.size());
    IabGdprUserConsent::convertBytesToBitset(decodedCoreString, allBitSet);

    if (!parseCoreStringData(allBitSet, readAtPos, context.m_coreStringData))
    {
        context.errorCode = errorInDecodingCoreString;
        return;
    }

    for (size_t i = 1; i < consentsParts.size(); i++)
    {
        readAtPos = 0;

        std::string decodedUnknownPart = MM::Utils::base64Decode(consentsParts.at(i), true);
        boost::dynamic_bitset<> bitsetPart(8 * decodedUnknownPart.size());
        IabGdprUserConsent::convertBytesToBitset(decodedUnknownPart, bitsetPart);

        if (IabGdprUserConsentSingleton::instance().decodeOptionalParts || decodeOptionalParts) {
            parseOptionalParts(bitsetPart, context, readAtPos);
        }
    }
}

void MM::Privacy::TCFV2Decoder::parseOptionalParts(const boost::dynamic_bitset<>& bitsetPart,
                                                   TCFV2Context& context,
                                                   uint32_t& readAtPos) {
    long segmentType = Util::getLongMoveIndex(bitsetPart, SEGMENT_TYPE_LENGTH, readAtPos);
    if (segmentType < 0)
    {
        context.errorCode = badSegmentType;
        return;
    }
    switch (segmentType) {
        case 1:
            if (!context.m_disclosedVendorListInfo)
            {
                context.m_disclosedVendorListInfo = new std::unordered_map<uint32_t, bool>();
            }
            if (!parseSectionWithBitFieldOrRange(bitsetPart, readAtPos, *context.m_disclosedVendorListInfo))
            {
                context.errorCode = errorInDecodingDisclosedVendors;
                return;
            }
            break;
        case 2:
            if (!context.m_allowedVendorListInfo)
            {
                context.m_allowedVendorListInfo = new std::unordered_map<uint32_t, bool>();
            }
            if (!parseSectionWithBitFieldOrRange(bitsetPart, readAtPos, *context.m_allowedVendorListInfo))
            {
                context.errorCode = errorInDecodingAllowedVendorsList;
                return;
            }
            break;
        case 3:
            // we don't parse publisherTC segment because we don't use it in bidder
            break;
        default:
            break;

    }
}

bool MM::Privacy::TCFV2Decoder::parseCoreStringData(const boost::dynamic_bitset<>& allBitSet,
                                                    uint32_t& readAtPos, CoreStringData& data)
{

    if (!parseCoreStringGeneralSection(allBitSet, readAtPos, data)) {
        return false;
    }

    if (!parseSectionWithBitFieldOrRange(allBitSet, readAtPos, data.m_vendorConsents)) {
        return false;
    }

    if (!parseSectionWithBitFieldOrRange(allBitSet, readAtPos, data.m_vendorInterests)) {
        return false;
    }

    if (!parsePublisherRestrictionSection(allBitSet, readAtPos, data)) {
        return false;
    }

    return true;
}

bool TCFV2Decoder::parseCoreStringGeneralSection(const boost::dynamic_bitset<>& allBitSet,
                                                 uint32_t& readAtPos, CoreStringData& data)
{

    data.m_version = Util::getLongMoveIndex(allBitSet, CoreStringData::VERSION_LENGTH, readAtPos);
    if (data.m_version <= 0) {
        return false;
    }

    data.m_createdAt = Util::getLongMoveIndex(allBitSet, CoreStringData::CREATE_LENGTH, readAtPos);
    if (data.m_createdAt < 0)
    {
        return false;
    }
    data.m_lastUpdated = Util::getLongMoveIndex(allBitSet, CoreStringData::UPDATED_LENGTH, readAtPos);
    if (data.m_lastUpdated < 0)
    {
        return false;
    }

    data.m_cmpId = Util::getLongMoveIndex(allBitSet, CoreStringData::CMP_ID_LENGTH, readAtPos);
    if (data.m_cmpId < 0)
    {
        return false;
    }

    data.m_cmpVersion = Util::getLongMoveIndex(allBitSet, CoreStringData::CMP_VERSION_LENGTH, readAtPos);
    if (data.m_cmpVersion < 0)
    {
        return false;
    }

    data.m_consentScreen = Util::getLongMoveIndex(allBitSet, CoreStringData::CONSENT_SCREEN_LENGTH, readAtPos);
    if (data.m_consentScreen < 0)
    {
        return false;
    }

    long charIndex = Util::getLongMoveIndex(allBitSet, CoreStringData::CONSENT_LANGUAGE_LENGTH, readAtPos);
    if (charIndex < 0)
    {
        return false;
    }

    char cl1 = (char)(charIndex + 65);

    long char2Index = Util::getLongMoveIndex(allBitSet, CoreStringData::CONSENT_LANGUAGE_LENGTH, readAtPos);
    if (char2Index < 0)
    {
        return false;
    }

    char cl2 = (char)(char2Index + 65);

    data.m_consentLanguage.push_back(cl1);
    data.m_consentLanguage.push_back(cl2);

    data.m_vendorListVersion = Util::getLongMoveIndex(allBitSet, CoreStringData::VENDOR_LIST_VERSION_LENGTH, readAtPos);
    if (data.m_vendorListVersion < 0)
    {
        return false;
    }

    data.m_tcfPolicyVersion = Util::getLongMoveIndex(allBitSet, CoreStringData::TCF_POLICY_VERSION_LENGTH, readAtPos);
    if (data.m_tcfPolicyVersion < 0)
    {
        return false;
    }

    long isServiceSpecific = Util::getLongMoveIndex(allBitSet, CoreStringData::IS_SERVER_SPECIFIC_LENGTH, readAtPos);
    if (isServiceSpecific < 0)
    {
        return false;
    }
    data.m_isServiceSpecific = (bool)isServiceSpecific;

    long useNonStandardStacks = Util::getLongMoveIndex(allBitSet,
                                                       CoreStringData::USE_NON_STANDARD_LENGTH, readAtPos);
    if (useNonStandardStacks < 0)
    {
        return false;
    }
    data.m_useNonStandardStacks = (bool)useNonStandardStacks;

    if (!Util::populateBitsetAndMoveIndex(allBitSet, readAtPos,
                                          CoreStringData::SPECIAL_FEATURE_OPT_INS_LENGTH,
                                          data.m_specialFeatureOptIns))
    {
        return false;
    }

    if (!Util::populateBitsetAndMoveIndex(allBitSet,
                                          readAtPos,
                                          CoreStringData::PURPOSE_CONTENT_LENGTH,
                                          data.m_purposesConsent))
    {
        return false;
    }

    if (!Util::populateBitsetAndMoveIndex(allBitSet,
                                          readAtPos,
                                          CoreStringData::PURPOSE_LI_TRANSPARENCY_LENGTH,
                                          data.m_purposesLITransparency))
    {
        return false;
    }

    long purposeOneTreatment = Util::getLongMoveIndex(allBitSet,
                                                      CoreStringData::PURPOSE_ON_TREATMENT_LENGTH, readAtPos);
    if (purposeOneTreatment < 0)
    {
        return false;
    }

    data.m_purposeOneTreatment = (bool)purposeOneTreatment;

    long pccl1Char = Util::getLongMoveIndex(allBitSet,
                                            CoreStringData::PUBLISHER_CC_LANGUAGE_LENGTH, readAtPos);
    if (pccl1Char < 0)
    {
        return false;
    }

    char pccl1 = (char)(pccl1Char + 65);

    long pccl2Char = Util::getLongMoveIndex(allBitSet,
                                            CoreStringData::PUBLISHER_CC_LANGUAGE_LENGTH, readAtPos);
    if (pccl2Char < 0)
    {
        return false;
    }

    char pccl2 = (char)(pccl2Char + 65);

    data.m_publisherCC.push_back(pccl1);
    data.m_publisherCC.push_back(pccl2);

    return true;
}

bool TCFV2Decoder::parseSectionWithBitFieldOrRange(const boost::dynamic_bitset<>& allBitSet,
                                                   uint32_t& readAtPos,
                                                   std::unordered_map<uint32_t, bool>& consentMap)
{

    long maxVendorId = Util::getLongMoveIndex(allBitSet, MAX_VENDOR_ID_LENGTH, readAtPos);
    if (maxVendorId < 0) {
        return false;
    }

    long isRangeEncoding = Util::getLongMoveIndex(allBitSet, IS_RANGE_ENCODING_LENGTH, readAtPos);
    if (isRangeEncoding < 0) {
        return false;
    }

    if (isRangeEncoding == 1) {
        long numberOfEntries = Util::getLongMoveIndex(allBitSet, NUMBER_OF_ENTRIES_LENGTH, readAtPos);
        if (numberOfEntries < 0) {
            return false;
        }

        for (int i = 0; i < numberOfEntries; i++) {
            // parsing rangeEntries
            long isRangeIndex = Util::getLongMoveIndex(allBitSet, IS_RANGE_LENGTH, readAtPos);
            if (isRangeIndex < 0) {
                return false;
            }

            bool isRange = (bool)isRangeIndex;

            if (isRange)
            {

                // MULTIRANGE
                long startOrOnlyVendorId = Util::getLongMoveIndex(allBitSet, START_OR_ONLY_VENDOR_ID_LENGTH, readAtPos);
                if (startOrOnlyVendorId < 0) {
                    return false;
                }

                long endVendorId = Util::getLongMoveIndex(allBitSet, START_OR_ONLY_VENDOR_ID_LENGTH, readAtPos);
                if (endVendorId < 0) {
                    return false;
                }

                // so from startOrOnlyVendorId to endVendorId they have consent. we should fill the map
                if (startOrOnlyVendorId > endVendorId) {
                    return false;
                }

                if (endVendorId > maxVendorId) {
                    return false;
                }

                for (int i = startOrOnlyVendorId; i <= endVendorId; i++) {
                    consentMap[i] = true;
                }
            } else {
                // is not a range we have only one vendor id
                // we set the value for that vendor id to true and the rest will be deemed as false
                long startOrOnlyVendorId = Util::getLongMoveIndex(allBitSet, START_OR_ONLY_VENDOR_ID_LENGTH, readAtPos);
                if (startOrOnlyVendorId < 0) {
                    return false;
                }
                if (startOrOnlyVendorId > maxVendorId) {
                    return false;
                }

                consentMap[startOrOnlyVendorId] = true;
            }
        }
    } else {
        boost::dynamic_bitset<> vendorConsentBitset(maxVendorId);
        if (!Util::populateBitsetAndMoveIndex(allBitSet, readAtPos, maxVendorId, vendorConsentBitset)) {
            return false;
        }

        for (std::size_t i = 0; i < vendorConsentBitset.size(); i++) {
            //we only set the vendor with true consent in consent map
            if (vendorConsentBitset[i])
            {
                // bit i corresponds to vendor i+1 in consent map
                uint32_t vendorIndex = i + 1;
                consentMap[vendorIndex] = true;
            }
        }
    }

    return true;
}

bool TCFV2Decoder::parsePublisherRestrictionSection(const boost::dynamic_bitset<>& allBitSet,
                                                    uint32_t& readAtPos, CoreStringData& data)
{
    //The content of this section is optional EXCEPT for NumPubRestrictions.
    // Encodes any number of single or range restriction entries
    long numPubRestrictions = Util::getLongMoveIndex(allBitSet, NUMB_PUB_RESTRICTIONS_LENGTH, readAtPos);
    if (numPubRestrictions < 0)
    {
        return false;
    }
    if (!data.purposeIdToPublisherRestriction)
    {
        data.purposeIdToPublisherRestriction = new std::unordered_map<std::pair<uint32_t, uint32_t>,
                PubRestrictionEntry*, boost::hash<std::pair<uint32_t, uint32_t>>>();
    }

    for (int i = 0; i < numPubRestrictions; i++)
    {
        //PubRestrictionEntry
        long purposeId = Util::getLongMoveIndex(allBitSet, PURPOSE_ID_LENGTH, readAtPos);
        if (purposeId < 0) {
            return false;
        }

        long restrictionType = Util::getLongMoveIndex(allBitSet, PUB_RESTRICTION_TYPE_LENGTH, readAtPos);
        if (restrictionType < 0 || restrictionType > 3) {
            return false;
        }

        auto ptr = data.purposeIdToPublisherRestriction->find(std::make_pair(purposeId, restrictionType));
        PubRestrictionEntry *entry;
        if (ptr == data.purposeIdToPublisherRestriction->end()) {
            entry = new PubRestrictionEntry();
            entry->m_purposeId = purposeId;
            entry->m_restrictionType = static_cast<PubRestrictionType>(restrictionType);

            data.purposeIdToPublisherRestriction->insert(std::make_pair(std::make_pair(purposeId, restrictionType), entry));
        }
        else {
            entry = ptr->second;
        }

        long numberOfEntries = Util::getLongMoveIndex(allBitSet, NUMBER_OF_ENTRIES_LENGTH, readAtPos);
        if (numberOfEntries < 0) {
            return false;
        }
        if (!entry->m_restrictedVendors) {
            entry->m_restrictedVendors = new std::unordered_set<uint32_t>();
        }
        for (int i = 0; i < numberOfEntries; i++) {

            long isRange = Util::getLongMoveIndex(allBitSet, IS_RANGE_LENGTH, readAtPos);
            if (isRange < 0) {
                return false;
            }

            if (isRange) {
                //MULTIRANGE
                long startOrOnlyVendorId = Util::getLongMoveIndex(allBitSet, START_OR_ONLY_VENDOR_ID_LENGTH, readAtPos);
                if (startOrOnlyVendorId < 0)
                {
                    return false;
                }

                long endVendorId = Util::getLongMoveIndex(allBitSet, START_OR_ONLY_VENDOR_ID_LENGTH, readAtPos);
                if (endVendorId < 0)
                {
                    return false;
                }

                if (startOrOnlyVendorId > endVendorId)
                {
                    return false;
                }

                entry->m_restrictedVendors->reserve(endVendorId - startOrOnlyVendorId + 1);
                for (uint32_t i = startOrOnlyVendorId; i <= endVendorId; i++)
                {
                    entry->m_restrictedVendors->insert(i);
                }
            }
            else
            {
                //only one vendor id is provided
                long startOrOnlyVendorId = Util::getLongMoveIndex(allBitSet, START_OR_ONLY_VENDOR_ID_LENGTH, readAtPos);
                if (startOrOnlyVendorId < 0)
                {
                    return false;
                }

                entry->m_restrictedVendors->insert(startOrOnlyVendorId);
            }
        } // end of one RangeEntry

    } // end of one PubRestrictionEntry

    return true;
}

MM::Privacy::PubRestrictionEntry::~PubRestrictionEntry()
{
    delete m_restrictedVendors;
}

MM::Privacy::CoreStringData::~CoreStringData()
{

    if (purposeIdToPublisherRestriction)
    {
        for (auto &kv : *purposeIdToPublisherRestriction)
        {
            delete kv.second;
        }
    }
    delete purposeIdToPublisherRestriction;
}

MM::Privacy::TCFV2Context::~TCFV2Context() {
    delete m_allowedVendorListInfo;
    delete m_disclosedVendorListInfo;
}

void MM::Privacy::TCFV2Context::reportMap(MM::Utils::MemBuffer& out,
                                          const std::unordered_map<uint32_t, bool>& map) const
{
    uint32_t maxId = 0;
    for (auto ii = map.begin(); ii != map.end(); ++ii) {
        if ((*ii).first > maxId) {
            maxId = (*ii).first;
        }
    }
    for (uint32_t i = 1; i <= maxId; i++) {
        auto ptr = map.find(i);
        if (ptr != map.end()) {
            out << i << ":<code>" << (ptr->second == true ? "true" : "false") << "</code>\n";
        }
        else {
            out << i << ":<code>" << "false" << "</code>\n";
        }

    }
}

uint32_t MM::Privacy::TCFV2Context::getVersion() const
{
    return m_coreStringData.m_version;
}

void MM::Privacy::TCFV2Context::report(MM::Utils::MemBuffer& out) const
{
    if (errorCode != noError)
    {
        out << "<br/>Failed to decode!!!<br>"
            << "errorCode: <code>" << errorCode << "</code><br>\n"
            << "errorName: <code>" << Util::getErrorTypeName(errorCode) << "</code><br>\n";

        return;
    }

    auto rescritions = Util::getPrivacyRestrictionInfo(privacyRestrictions);
    out << "Decoded successfully<br>\n"
        << "PrivacyRestriction: <br>";
    for (auto rest : rescritions) {
        out<<"<code> " << rest << " </code > <br>\n ";
    }

    out << "<br><u><b>Consent Core String :</b></u><br><br>\n"
        << "Version: <code>" << m_coreStringData.m_version << "</code><br>\n"
        << "Created on : <code>(" << MM::Utils::TimeString().gmTimeForHTML((size_t)(m_coreStringData.m_createdAt / 10.0)) << ")</code><br>\n"
        << "Last Updated on : <code>(" << MM::Utils::TimeString().gmTimeForHTML((size_t)(m_coreStringData.m_lastUpdated / 10.0)) << ")</code><br>\n"
        << "Cmp Id: <code>" << m_coreStringData.m_cmpId << "</code><br>\n"
        << "Cmp Version: <code>" << m_coreStringData.m_cmpVersion << "</code><br>\n"
        << "Consent Screen: <code>" << m_coreStringData.m_consentScreen << "</code><br>\n"
        << "Consent Language: <code>" << m_coreStringData.m_consentLanguage << "</code><br>\n"
        << "Vendor List Version: <code>" << m_coreStringData.m_vendorListVersion << "</code><br>\n"
        << "TCF Policy Version: <code>" << m_coreStringData.m_tcfPolicyVersion << "</code><br>\n"
        << "Is Server Specific: <code>" << (m_coreStringData.m_isServiceSpecific ? "true" : "false") << "</code><br>\n"
        << "Use Non Standard Stacks: <code>" << (m_coreStringData.m_useNonStandardStacks ? "true" : "false") << "</code><br>\n"
        << " Specific Jurisdiction Disclosures: <br>\n"
        << "Purpose On Treatment: <code>" << (m_coreStringData.m_purposeOneTreatment ? "true" : "false") << "</code><br>\n"
        << "Publisher Country Code: <code>" << m_coreStringData.m_publisherCC << "</code><br>\n"
        << "Special Feature Opt ins: <br>\n";
    for (std::size_t i = 0; i < m_coreStringData.m_specialFeatureOptIns.size(); i++)
    {
        out << (i + 1) << ":<code>" << (m_coreStringData.m_specialFeatureOptIns[i] == true ? "true" : "false") << "</code><br>\n";
    }
    out << "<br>\n";
    out << "Purposes Consent: <br>\n";
    for (std::size_t i = 0; i < m_coreStringData.m_purposesConsent.size(); i++)
    {
        out << (i + 1) << ":<code>" << (m_coreStringData.m_purposesConsent[i] == true ? "true" : "false") << "</code><br>\n";
    }
    out << "<br>\n";
    out << "Purpose Legitimate Interest: <br>\n";
    for (std::size_t i = 0; i < m_coreStringData.m_purposesLITransparency.size(); i++)
    {
        out << (i + 1) << ":<code>" << (m_coreStringData.m_purposesLITransparency[i] == true ? "true" : "false") << "</code><br>\n";
    }
    out << "<br>\n";
    out << "Vendor Consent Section <br>\n";
    out << m_mapDivStyle;
    reportMap(out, m_coreStringData.m_vendorConsents);
    out << "</div>";
    out << "<br>\n";

    out << "Vendor Legitimate Interest Section <br>\n";
    out << m_mapDivStyle;
    reportMap(out, m_coreStringData.m_vendorInterests);
    out << "</div>";
    out << "<br>\n";

    if (m_coreStringData.purposeIdToPublisherRestriction &&
        !m_coreStringData.purposeIdToPublisherRestriction->empty())
    {
        out << "Publisher restrictions : <br>\n";
        uint32_t maxPurposeId = 0;
        for (auto ii = m_coreStringData.purposeIdToPublisherRestriction->begin();
             ii != m_coreStringData.purposeIdToPublisherRestriction->end(); ++ii)
        {
            if ((*ii).first.first > maxPurposeId)
            {
                maxPurposeId = (*ii).first.first;
            }
        }
        for (uint32_t i = 0; i <= maxPurposeId; i++) {
            for (uint32_t restrictionType = 0; restrictionType < 4; restrictionType++) {
                auto ptr = m_coreStringData.purposeIdToPublisherRestriction->find(std::make_pair(i, restrictionType));
                if (ptr != m_coreStringData.purposeIdToPublisherRestriction->end()) {
                    auto restriction = ptr->second;
                    out << "<br> Purpose Id: <code>" << restriction->m_purposeId << "</code><br>\n";
                    out << " Restriction Type Number : <code>" << restriction->m_restrictionType << "</code><br>\n";
                    out << " Restriction Type Name : <code>" << Util::getPubRestrictionTypeName(restriction->m_restrictionType) << "</code><br>\n";
                    out << " Restricted Vendors: <br>\n";
                    if (restriction->m_restrictedVendors) {
                        for (auto ptr : *restriction->m_restrictedVendors) {
                            out << ptr << "<br>";
                        }
                    }
                }
            }
        }
    }
    if (m_disclosedVendorListInfo) {
        out << "<br>\n";
        out << "Disclosed Vendor List: <br><br>";
        out << m_mapDivStyle;
        reportMap(out, *m_disclosedVendorListInfo);
        out << "</div>";
    }

    if (m_allowedVendorListInfo) {
        out << "<br>\n";
        out << "Allowed Vendor List: <br><br>\n";
        out << "<div style=\"display: grid; grid-template-columns: 20px 50px; grid-gap: 12px;max-height: 300px;overflow: auto; \">";
        reportMap(out, *m_allowedVendorListInfo);
        out << "</div>";
    }

    out << "<br>\n";
}

bool MM::Privacy::Util::populateBitsetAndMoveIndex(const boost::dynamic_bitset<>& src,
                                                   uint32_t& start,
                                                   const uint32_t size,
                                                   boost::dynamic_bitset<>& dest) {
    if (src.size() < start + size) {
        // src is not big enough to read all request bits
        return false;
    }
    for (uint32_t i = 0; i < size; i++) {
        dest[i] = src[i + start];
    }

    start += size;
    return true;
}

// TODO this is a bug in production in iabGdprUserConsent.cpp, fix it and merge it
// with this function when working on the Gdpr V1 bug ticket.
// we need 'if ((start + num_bits) > (int) bitSet.size()) {'
// test PARSE_VENDOR_ALLOWED.PARSE_VENDORS_STRING will confirm that
long MM::Privacy::Util::getLongAtPosition(const boost::dynamic_bitset<>& bitset,
                                          const uint32_t num_bits,
                                          const uint32_t start)
{
    if ((start + num_bits) > bitset.size() || num_bits >= sizeof(long) * 8)
    {
        return -1;
    }
    long longRes = 0;
    // capture set bits from start to start + num_bits
    // going backwards, because we read binary right to left
    for (uint32_t i = start; i < start + num_bits; i++)
    {
        longRes <<= 1;
        if (bitset.test(i))
        {
            longRes |= 1;
        }
    }
    return longRes;
}

MM::Privacy::PrivacyService::PrivacyService() : m_mmAllowedToBidCntr("MM Allowed To Bid", 1000),
                                                m_mmNotAllowedToBidCntr("MM Not Allowed To Bid", 1001),
                                                m_vendorSyncAllowedCntr("Vendor Sync Allowed", 1002),
                                                m_vendorSyncNotAllowedCntr("Vendor Sync NotAllowed", 1003),
                                                m_noRestrictionsCntr("[Privacy Restriction] No Restrictions", 1004),
                                                m_noPreciseGeolocationUsageCntr("[Privacy Restriction] No Geolocation Usage in Bidding", 1005),
                                                m_fullyRestrictedCntr("[Privacy Restriction] Fully Restricted", 1006),
                                                m_blockeByPubRestrictionCntr("Blocked By Publisher Restriction", 1007),
                                                m_contextHasErrorCntr("TCFV2 parsing error", 1008) {
    m_allV2Cntrs.push_back(&m_mmAllowedToBidCntr);
    m_allV2Cntrs.push_back(&m_mmNotAllowedToBidCntr);
    m_allV2Cntrs.push_back(&m_vendorSyncAllowedCntr);
    m_allV2Cntrs.push_back(&m_vendorSyncNotAllowedCntr);
    m_allV2Cntrs.push_back(&m_noRestrictionsCntr);
    m_allV2Cntrs.push_back(&m_noPreciseGeolocationUsageCntr);
    m_allV2Cntrs.push_back(&m_fullyRestrictedCntr);
    m_allV2Cntrs.push_back(&m_blockeByPubRestrictionCntr);
    m_allV2Cntrs.push_back(&m_contextHasErrorCntr);
}

// Gets the PrivacyRestriction(s) based on context information.
void MM::Privacy::PrivacyService::getBiddingPrivacyRestrictions(const uint32_t& vendorId, TCFV2Context& context) {
    if (context.errorCode != noError) {
        m_contextHasErrorCntr.inc();
        getDecisionOnMalformedConsentString(context);
    } else if (IabGdprUserConsentSingleton::instance().useTransitionLogicV2) {
        getBiddingPrivacyRestrictionsInTransitionPeriod(vendorId, context);
    } else {
        getBiddingPrivacyRestrictionsInPostTransitionPeriod(vendorId, context);
    }
}

bool MM::Privacy::PrivacyService::isVendorFreeOfRequirement(
        const uint32_t& vendorId, const std::initializer_list<uint32_t> &purposeIds, PubRestrictionType restrictionType,
        const std::unordered_map<std::pair<uint32_t, uint32_t>, PubRestrictionEntry*, boost::hash<std::pair<uint32_t, uint32_t>>>& purposeIdToPublisherRestriction) const {
for (uint32_t pId : purposeIds) {
auto pair = purposeIdToPublisherRestriction.find(std::make_pair(pId, restrictionType));
if (pair != purposeIdToPublisherRestriction.end() &&
pair->second &&
        pair->second->m_restrictedVendors &&
        pair->second->m_restrictedVendors->find(vendorId) != pair->second->m_restrictedVendors->end()) {
return false;
}
}

return true;
}

void MM::Privacy::PrivacyService::getBiddingPrivacyRestrictionsInPostTransitionPeriod(const uint32_t& vendorId, TCFV2Context& context) {
    bool vendorConsentGiven = false;
    auto vcPair = context.m_coreStringData.m_vendorConsents.find(vendorId);
    if (vcPair != context.m_coreStringData.m_vendorConsents.end() && vcPair->second == true) {
        vendorConsentGiven = true;
    }

    bool vendorLIGiven = false;
    auto vLiPair = context.m_coreStringData.m_vendorInterests.find(vendorId);
    if (vLiPair != context.m_coreStringData.m_vendorInterests.end() && vLiPair->second == true) {
        vendorLIGiven = true;
    }
    bool checkPubRestriction = false;
    if (context.m_coreStringData.purposeIdToPublisherRestriction) {
        // we check if any of the purposeIds' publisher restrictions are disallowing our vendor
        bool allPurposesAreAllowed = isVendorFreeOfRequirement(vendorId, {1, 2, 3, 4, 7}, not_allowed, *context.m_coreStringData.purposeIdToPublisherRestriction);
        if (!allPurposesAreAllowed) {
            context.privacyRestrictions.insert(fully_restricted);
            return;
        }
        checkPubRestriction = true;
    }
    // 1. Consent established as a legal basis for Purposes [1, 3, 4] and Special Features [1];
    // 2. Legitimate Interest established for Purposes [2, 7]:
    bool case1 = vendorConsentGiven &&
                 (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {1, 3, 4}, requires_legitimate_interest, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
                 context.m_coreStringData.m_purposesConsent[0] == true &&
                 context.m_coreStringData.m_purposesConsent[2] == true &&
                 context.m_coreStringData.m_purposesConsent[3] == true &&
                 context.m_coreStringData.m_specialFeatureOptIns[0] == true;

    bool case2 = vendorLIGiven &&
                 (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {2, 7}, requires_consent, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
                 context.m_coreStringData.m_purposesLITransparency[1] == true &&
                 context.m_coreStringData.m_purposesLITransparency[6] == true;

    if (case1 && case2) {
        context.privacyRestrictions.insert(no_restrictions);
        return;
    }

    // 1.Consent established as a legal basis for Purposes [1, 3, 4];
    // 2. Legitimate Interest established for Purposes [2, 7]
    case1 = vendorConsentGiven &&
            (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {1, 3, 4}, requires_legitimate_interest, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
            context.m_coreStringData.m_purposesConsent[0] == true &&
            context.m_coreStringData.m_purposesConsent[2] == true &&
            context.m_coreStringData.m_purposesConsent[3] == true;

    if (case1 && case2) {
        context.privacyRestrictions.insert(no_precise_geo_location_usage);
        return;
    }


    context.privacyRestrictions.insert(fully_restricted);
}

void MM::Privacy::PrivacyService::getBiddingPrivacyRestrictionsInTransitionPeriod(const uint32_t& vendorId, TCFV2Context& context) {

    bool vendorConsentGiven = false;
    auto vcPair = context.m_coreStringData.m_vendorConsents.find(vendorId);
    if (vcPair != context.m_coreStringData.m_vendorConsents.end() && vcPair->second == true) {
        vendorConsentGiven = true;
    }

    bool vendorLIGiven = false;
    auto vLiPair = context.m_coreStringData.m_vendorInterests.find(vendorId);
    if (vLiPair != context.m_coreStringData.m_vendorInterests.end() && vLiPair->second == true) {
        vendorLIGiven = true;
    }

    bool checkPubRestriction = false;
    if (context.m_coreStringData.purposeIdToPublisherRestriction) {
        // we check if any of the purposeIds' publisher restrictions are disallowing our vendor
        bool allPurposesAreAllowed = isVendorFreeOfRequirement(vendorId, {1, 2, 3, 4, 7}, not_allowed, *context.m_coreStringData.purposeIdToPublisherRestriction);
        if (!allPurposesAreAllowed) {
            context.privacyRestrictions.insert(fully_restricted);
            return;
        }
        checkPubRestriction = true;
    }

    // 1. Consent established as a legal basis for Purposes [1] and Special Features [1];
    // 2. Consent or Legitimate Interest established as a legal basis for Purposes [3 & 4];
    // 3. Legitimate Interest established for Purposes [2, 7]
    bool case1 = vendorConsentGiven &&
                 (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {1}, requires_legitimate_interest, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
                 context.m_coreStringData.m_purposesConsent[0] == true &&
                 context.m_coreStringData.m_specialFeatureOptIns[0] == true;

    // case2 is the most complicated case, this is what was given to us by DP&G team
    // (P3 = C && P4 = C) || (P3 = LI && P4 = LI) || (P3 = C && P4 = LI) || (P3 = LI && P4 = C);
    bool case2 =
            // case : (P3 = C && P4 = C)
            ((checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {3, 4}, requires_legitimate_interest, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
             vendorConsentGiven && context.m_coreStringData.m_purposesConsent[2] == true &&
             context.m_coreStringData.m_purposesConsent[3] == true) ||
            // case : (P3 = LI && P4 = LI)
            ((checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {3, 4}, requires_consent, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
             vendorLIGiven && context.m_coreStringData.m_purposesLITransparency[2] == true &&
             context.m_coreStringData.m_purposesLITransparency[3] == true) ||

            // case : (P3 = C && P4 = LI)
            ((vendorConsentGiven && context.m_coreStringData.m_purposesConsent[2] == true &&
              (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {3}, requires_legitimate_interest, *context.m_coreStringData.purposeIdToPublisherRestriction) : true))
             &&
             (vendorLIGiven && context.m_coreStringData.m_purposesLITransparency[3] == true &&
              (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {4}, requires_consent, *context.m_coreStringData.purposeIdToPublisherRestriction) : true)))
            ||
            // case : (P3 = LI && P4 = C)
            ((vendorLIGiven && context.m_coreStringData.m_purposesLITransparency[2] == true &&
              (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {3}, requires_consent, *context.m_coreStringData.purposeIdToPublisherRestriction) : true))
             &&
             (vendorConsentGiven && context.m_coreStringData.m_purposesConsent[3] == true &&
              (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {4}, requires_legitimate_interest, *context.m_coreStringData.purposeIdToPublisherRestriction) : true)));

    bool case3 =
            (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {2, 7}, requires_consent, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
            vendorLIGiven && context.m_coreStringData.m_purposesLITransparency[1] == true &&
            context.m_coreStringData.m_purposesLITransparency[6] == true;

    if (case1 && case2 && case3) {
        context.privacyRestrictions.insert(no_restrictions);
        return;
    }

    // 1. Consent established as a legal basis for Purposes [1];
    // 2. Consent or Legitimate Interest established as legal basis for Purposes [3 & 4];
    // 3. Legitimate Interest established for Purposes [2, 7]
    case1 = (checkPubRestriction ? isVendorFreeOfRequirement(vendorId, {1}, requires_legitimate_interest, *context.m_coreStringData.purposeIdToPublisherRestriction) : true) &&
            vendorConsentGiven && context.m_coreStringData.m_purposesConsent[0] == true;
    if (case1 && case2 && case3) {
        context.privacyRestrictions.insert(no_precise_geo_location_usage);
        return;
    }

    context.privacyRestrictions.insert(fully_restricted);
}

bool MM::Privacy::PrivacyService::isVendorUserSyncAllowed(const uint32_t& vendorId, TCFV2Context& context) {
    if (context.privacyRestrictions.empty()) {
        //TODO update this to getSyncingPrivacyRestrictions after Will gives us requirements
        getBiddingPrivacyRestrictions(vendorId, context);
        countRestrictions(context.privacyRestrictions);
    }

    bool isVendorSyncAllowed =
            context.privacyRestrictions.find(no_restrictions) != context.privacyRestrictions.end();
    if (isVendorSyncAllowed) {
        m_vendorSyncAllowedCntr.inc();
    } else {
        m_vendorSyncNotAllowedCntr.inc();
    }
    return isVendorSyncAllowed;
}

bool MM::Privacy::PrivacyService::isVendorAllowedToBid(const uint32_t gvId, TCFV2Context& context)
{
    if (context.privacyRestrictions.empty()) {
        getBiddingPrivacyRestrictions(gvId, context);
        countRestrictions(context.privacyRestrictions);
    }

    bool isMMAllowed =
            context.privacyRestrictions.find(no_restrictions) != context.privacyRestrictions.end();
    if (isMMAllowed) {
        m_mmAllowedToBidCntr.inc();
    } else {
        m_mmNotAllowedToBidCntr.inc();
    }
    return isMMAllowed;
}

void MM::Privacy::PrivacyService::countRestrictions(const RestrictionSetType& restricitons) {

    for (auto rest : restricitons) {
        switch (rest) {
            case no_restrictions:
                m_noRestrictionsCntr.inc();
                break;
            case no_precise_geo_location_usage:
                m_noPreciseGeolocationUsageCntr.inc();
                break;
            case fully_restricted:
                m_fullyRestrictedCntr.inc();
                break;
            default:
                break;
        }
    }
}

void MM::Privacy::PrivacyService::getDecisionOnMalformedConsentString(TCFV2Context& context) {
    // in transition period we set this config to true
    bool bidOnBadDaisybit = BidderConfig::instance().getGDPR().bidOnBadDaisybit;
    if (!bidOnBadDaisybit) {
        context.privacyRestrictions.insert(fully_restricted);
    } else {
        context.privacyRestrictions.insert(no_restrictions);
    }
}

std::vector<MM::Utils::StatCounter*>* MM::Privacy::PrivacyService::getOverallCntrs() {
    return &m_OverallCntrs;
}

std::vector<MM::Utils::StatCounter*> MM::Privacy::PrivacyService::getAllV2Cntrs() const {
    return m_allV2Cntrs;
}

long MM::Privacy::Util::getLongMoveIndex(const boost::dynamic_bitset<>& bitset, const uint32_t num_bits, uint32_t& start) {
    long res = getLongAtPosition(bitset, num_bits, start);
    if (res < 0) {
        return -1;
    }
    // move pointer forward for the field, and then move pointer
    // for the 8 bit padding
    start += num_bits;
    return res;
}

PrivacyService *MM::Privacy::PrivacyService::instance()
{
    if (m_inst == 0)
    {
        std::lock_guard<std::mutex> guard(s_mutex);
        if (m_inst == 0)
        {
            m_inst = new PrivacyService();
        }
    }
    return m_inst;
}
