### What is this project about?
    This is an implementation of tcfv2 and gdpr v1 protocols for advertising

### How to build this project?

#### How to run this app?
    docker-compose up
    docker  exec -it 93451e5e2538 /bin/bash
    cd /home/privacy_service_gdpr_tcfv2
    cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja
    ./v2test.out
    ./v1test.out
### TO Dos for this project

