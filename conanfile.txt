[requires]

Poco/1.9.0@pocoproject/stable
boost/1.71.0@conan/stable
rapidjson/1.1.0@bincrafters/stable
tbb/2019_u9

[generators]
cmake
